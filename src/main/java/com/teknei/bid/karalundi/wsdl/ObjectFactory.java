
package com.teknei.bid.karalundi.wsdl;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.teknei.bid.wsdl package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _CreatePrintList_QNAME = new QName("http://ws.kivox.soran.com/", "createPrintList");
    private final static QName _AddSpeakersToListResponse_QNAME = new QName("http://ws.kivox.soran.com/", "addSpeakersToListResponse");
    private final static QName _DeletePrintList_QNAME = new QName("http://ws.kivox.soran.com/", "deletePrintList");
    private final static QName _RemoveSpeakersToList_QNAME = new QName("http://ws.kivox.soran.com/", "removeSpeakersToList");
    private final static QName _DeletePrintListResponse_QNAME = new QName("http://ws.kivox.soran.com/", "deletePrintListResponse");
    private final static QName _RemoveSpeakersToListResponse_QNAME = new QName("http://ws.kivox.soran.com/", "removeSpeakersToListResponse");
    private final static QName _CreatePrintListResponse_QNAME = new QName("http://ws.kivox.soran.com/", "createPrintListResponse");
    private final static QName _AddSpeakersToList_QNAME = new QName("http://ws.kivox.soran.com/", "addSpeakersToList");
    private final static QName _AuthenticationFault_QNAME = new QName("http://ws.kivox.soran.com/", "AuthenticationFault");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.teknei.bid.wsdl
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link DeletePrintList }
     * 
     */
    public DeletePrintList createDeletePrintList() {
        return new DeletePrintList();
    }

    /**
     * Create an instance of {@link RemoveSpeakersToList }
     * 
     */
    public RemoveSpeakersToList createRemoveSpeakersToList() {
        return new RemoveSpeakersToList();
    }

    /**
     * Create an instance of {@link AddSpeakersToListResponse }
     * 
     */
    public AddSpeakersToListResponse createAddSpeakersToListResponse() {
        return new AddSpeakersToListResponse();
    }

    /**
     * Create an instance of {@link CreatePrintList }
     * 
     */
    public CreatePrintList createCreatePrintList() {
        return new CreatePrintList();
    }

    /**
     * Create an instance of {@link DeletePrintListResponse }
     * 
     */
    public DeletePrintListResponse createDeletePrintListResponse() {
        return new DeletePrintListResponse();
    }

    /**
     * Create an instance of {@link CreatePrintListResponse }
     * 
     */
    public CreatePrintListResponse createCreatePrintListResponse() {
        return new CreatePrintListResponse();
    }

    /**
     * Create an instance of {@link RemoveSpeakersToListResponse }
     * 
     */
    public RemoveSpeakersToListResponse createRemoveSpeakersToListResponse() {
        return new RemoveSpeakersToListResponse();
    }

    /**
     * Create an instance of {@link AddSpeakersToList }
     * 
     */
    public AddSpeakersToList createAddSpeakersToList() {
        return new AddSpeakersToList();
    }

    /**
     * Create an instance of {@link AddSpeakersToListRes }
     * 
     */
    public AddSpeakersToListRes createAddSpeakersToListRes() {
        return new AddSpeakersToListRes();
    }

    /**
     * Create an instance of {@link RemoveSpeakersFromListRes }
     * 
     */
    public RemoveSpeakersFromListRes createRemoveSpeakersFromListRes() {
        return new RemoveSpeakersFromListRes();
    }

    /**
     * Create an instance of {@link DeletePrintListRes }
     * 
     */
    public DeletePrintListRes createDeletePrintListRes() {
        return new DeletePrintListRes();
    }

    /**
     * Create an instance of {@link CreatePrintListRes }
     * 
     */
    public CreatePrintListRes createCreatePrintListRes() {
        return new CreatePrintListRes();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreatePrintList }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.kivox.soran.com/", name = "createPrintList")
    public JAXBElement<CreatePrintList> createCreatePrintList(CreatePrintList value) {
        return new JAXBElement<CreatePrintList>(_CreatePrintList_QNAME, CreatePrintList.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AddSpeakersToListResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.kivox.soran.com/", name = "addSpeakersToListResponse")
    public JAXBElement<AddSpeakersToListResponse> createAddSpeakersToListResponse(AddSpeakersToListResponse value) {
        return new JAXBElement<AddSpeakersToListResponse>(_AddSpeakersToListResponse_QNAME, AddSpeakersToListResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DeletePrintList }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.kivox.soran.com/", name = "deletePrintList")
    public JAXBElement<DeletePrintList> createDeletePrintList(DeletePrintList value) {
        return new JAXBElement<DeletePrintList>(_DeletePrintList_QNAME, DeletePrintList.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RemoveSpeakersToList }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.kivox.soran.com/", name = "removeSpeakersToList")
    public JAXBElement<RemoveSpeakersToList> createRemoveSpeakersToList(RemoveSpeakersToList value) {
        return new JAXBElement<RemoveSpeakersToList>(_RemoveSpeakersToList_QNAME, RemoveSpeakersToList.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DeletePrintListResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.kivox.soran.com/", name = "deletePrintListResponse")
    public JAXBElement<DeletePrintListResponse> createDeletePrintListResponse(DeletePrintListResponse value) {
        return new JAXBElement<DeletePrintListResponse>(_DeletePrintListResponse_QNAME, DeletePrintListResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RemoveSpeakersToListResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.kivox.soran.com/", name = "removeSpeakersToListResponse")
    public JAXBElement<RemoveSpeakersToListResponse> createRemoveSpeakersToListResponse(RemoveSpeakersToListResponse value) {
        return new JAXBElement<RemoveSpeakersToListResponse>(_RemoveSpeakersToListResponse_QNAME, RemoveSpeakersToListResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreatePrintListResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.kivox.soran.com/", name = "createPrintListResponse")
    public JAXBElement<CreatePrintListResponse> createCreatePrintListResponse(CreatePrintListResponse value) {
        return new JAXBElement<CreatePrintListResponse>(_CreatePrintListResponse_QNAME, CreatePrintListResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AddSpeakersToList }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.kivox.soran.com/", name = "addSpeakersToList")
    public JAXBElement<AddSpeakersToList> createAddSpeakersToList(AddSpeakersToList value) {
        return new JAXBElement<AddSpeakersToList>(_AddSpeakersToList_QNAME, AddSpeakersToList.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.kivox.soran.com/", name = "AuthenticationFault")
    public JAXBElement<String> createAuthenticationFault(String value) {
        return new JAXBElement<String>(_AuthenticationFault_QNAME, String.class, null, value);
    }

}
