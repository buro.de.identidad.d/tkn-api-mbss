
package com.teknei.bid.karalundi.wsdl;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para removeSpeakersToListResponse complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="removeSpeakersToListResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="return" type="{http://ws.kivox.soran.com/}removeSpeakersFromListRes" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "removeSpeakersToListResponseV2", propOrder = {
    "_return"
})
public class RemoveSpeakersToListResponse {

    @XmlElement(name = "return")
    protected RemoveSpeakersFromListRes _return;

    /**
     * Obtiene el valor de la propiedad return.
     * 
     * @return
     *     possible object is
     *     {@link RemoveSpeakersFromListRes }
     *     
     */
    public RemoveSpeakersFromListRes getReturn() {
        return _return;
    }

    /**
     * Define el valor de la propiedad return.
     * 
     * @param value
     *     allowed object is
     *     {@link RemoveSpeakersFromListRes }
     *     
     */
    public void setReturn(RemoveSpeakersFromListRes value) {
        this._return = value;
    }

}
