package com.teknei.bid.controller.rest;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;

import javax.xml.namespace.QName;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.Base64Utils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.google.common.hash.Hashing;
import com.google.gson.Gson;
import com.morpho.mbss.generic.wsdl.EncodePersonResponse;
import com.morpho.mbss.generic.wsdl.ErrorCode;
import com.morpho.mbss.generic.wsdl.FaceSample;
import com.morpho.mbss.generic.wsdl.FaceSampleType;
import com.morpho.mbss.generic.wsdl.FingerprintSampleType;
import com.morpho.mbss.generic.wsdl.FingerprintTemplates;
import com.morpho.mbss.generic.wsdl.IWSMbss;
import com.morpho.mbss.generic.wsdl.ImageFormatType;
import com.morpho.mbss.generic.wsdl.MbssService;
import com.morpho.mbss.generic.wsdl.Person;
import com.morpho.mbss.generic.wsdl.PersonToPersonMatchResponse;
import com.morpho.mbss.generic.wsdl.Registration;
import com.morpho.mbss.generic.wsdl.Request;
import com.morpho.mbss.generic.wsdl.Response;
import com.morpho.mbss.generic.wsdl.RoutingData;
import com.morpho.mbss.generic.wsdl.SequenceCheckError;
import com.morpho.mbss.generic.wsdl.SequenceCheckErrorItem;
import com.morpho.mbss.generic.wsdl.SequenceCheckErrorType;
import com.morpho.mbss.generic.wsdl.StillImage;
import com.morpho.util.WSHelper;
import com.teknei.bid.biometric.data.OperationResult;
import com.teknei.bid.controller.rest.helper.FacialController;
import com.teknei.bid.controller.rest.helper.FingerController2;
import com.teknei.bid.dto.Fingers;
import com.teknei.bid.dto.FingersBytes;
import com.teknei.bid.dto.HttpStatusImageWrapper;
import com.teknei.bid.dto.HttpStatusWrapper;
import com.teknei.bid.dto.request.RequestAuthenticateFacial;
import com.teknei.bid.dto.request.RequestCreate;
import com.teknei.bid.dto.request.RequestFacial;
import com.teknei.bid.dto.request.RequestFingers;
import com.teknei.bid.dto.request.RequestSlaps;
import com.teknei.bid.dto.response.BasicResponse;
import com.teknei.bid.dto.response.ComplexResponse;
import com.teknei.bid.dto.response.SimpleResponse;
import com.teknei.bid.util.DuplicateFingerException;
import com.teknei.bid.ws.util.MbssUtils;

import io.swagger.annotations.ApiOperation;

/**
 * Created by Amaro on 06/07/2017.
 */
@RestController
@RequestMapping(value = "/mbss")
public class MBSSController {

    private static final Logger log = LoggerFactory.getLogger(MBSSController.class);

    @Value("${biometric.engine}")
    private String engine;
    private static final String KARALUNDI = "karalundi";  
    
    @Value("${biometric.facialEnroll}")
    private Boolean facialEnroll;
    @Value("${biometric.facialIdentify}")
    private Boolean facialIdentify;
    
	@Value("${tkn.karalundi.resource.finger.isSendVerifyMFWorking}")
    private Boolean isSendVerifyMFWorking;
   
    //@Autowired
    //private WSInvoker wsInvoker;
    @Autowired
    private MbssUtils mbssUtils;
    
    @Autowired
	private FingerController2 fingerController;

    @Autowired
	private FacialController facialController;
    
    private Boolean isAutoEnroll = false;
    private List<String> listCurps = null;
    /**
     * Creates a resource in biometric system based on request values
     *
     * @param requestCreate
     * @return the response wrapper values
     */
    @ApiOperation(value = "Create a resource in biometric system. Type to send: {1 - Fingers Only | 2 - Facial Only | 3 - Fingers + Facial  " +
            " | 4 - Slaps Only | 6 - Slaps + Facial }")
    @RequestMapping(value = "/add", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = "application/json")
    public ResponseEntity<SimpleResponse> createRecord(@RequestBody RequestCreate requestCreate)
    {
        log.info("lblancas:: [tkn-api-mbss] :: "+this.getClass().getName()+".createRecord ");
        try 
        {
            Files.write(Paths.get("/home/archivo_json.json"), requestCreate.toString().getBytes());
        } catch (IOException e) {
            log.error("Error writting income file: {}", e.getMessage());
        }
        String typeRequested = requestCreate.getType();
        String id = requestCreate.getId();
        int typeReq = 0;
        if (typeRequested == null) {
            log.info("Returning: {} - {}", buildResponseMissingFields(), HttpStatus.BAD_REQUEST);
            return new ResponseEntity<>(buildResponseMissingFields(), HttpStatus.BAD_REQUEST);
        }
        try {
            typeReq = Integer.parseInt(typeRequested);
        } catch (NumberFormatException ne) {
            log.info("Returning: {} - {}", buildResponseMissingFields(), HttpStatus.BAD_REQUEST);
            return new ResponseEntity<>(buildResponseMissingFields(), HttpStatus.BAD_REQUEST);
        }
        HttpStatus responseStatus;
        log.info("Type requested: {}", typeReq);
        switch (typeReq) {
            case 1:
                //Fingers request 
            	//Metodo psra agregar dedos de la persona KARALUNDI---->>>  
            	if(engine.equals(KARALUNDI)) {//TODO FLUJO DEMO 
            		responseStatus = addMinuciasToKaralundi(requestCreate);
            	}else {
	            	//<<<--------
            		Gson n = new Gson();
            		log.info(">>>>>>>>>>>>>>>>>>>>>>>>>"+n.toJson(requestCreate));
					//MORPHO--->>>
	                responseStatus = mbssUtils.checkAndInsertPersonFromFingers(id, requestCreate.getFingers(), requestCreate.getImageType().toUpperCase());
            	}
                break;
            case 2:            
	            responseStatus = mbssUtils.checkAndInsertPersonFromFace(id, requestCreate.getFacial());
            	break;
            case 3:
                //Fingers and face
            	//TODO FLUJO SABADEL
            	if(engine.equals(KARALUNDI)) {//TODO 
            		responseStatus = addMinuciasAndFaceToKaralundi(requestCreate);
            	}else {
            		responseStatus = mbssUtils.checkAndInsertPersonFromFaceAndFingers(id, requestCreate.getFacial(), requestCreate.getFingers(), requestCreate.getImageType());
            	}
                break;
            case 4:
                responseStatus = mbssUtils.checkAndInsertPersonFromSlaps(id, requestCreate.getSlaps());
                break;
            case 6:
                responseStatus = mbssUtils.checkAndInsertPersonFromSlapsAndFace(id, requestCreate.getSlaps(), requestCreate.getFacial());
                break;
            default:
                return new ResponseEntity<>(buildResponseMissingFields(), HttpStatus.BAD_REQUEST);
        }
        SimpleResponse response = new SimpleResponse();
        response.setId(id);
        response.setStatus(responseStatus.toString());
        response.setTime(String.valueOf(System.currentTimeMillis() / 1000));
        log.info("Returning: {} - {}", response, responseStatus);
        return new ResponseEntity<>(response, responseStatus);
    }

    /**
     * Genera coigo hash de la persona en base a las huellas encontradas.
     * @param id
     * @return
     */
    @RequestMapping(value = "/getHash/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<String> getHashForPerson(@PathVariable String id) 
    { 
    	List<String> templateData = new ArrayList<>();
        log.info("lblancas:: [tkn-api-mbss] :: "+this.getClass().getName()+".getHashForPerson ");//TODO KARALUNDI-->
        if(engine.equals(KARALUNDI)) {
        	templateData.add(Base64Utils.encodeToString(Base64.getDecoder().decode(fingerController.getTempHuella())));
        }else {
	        Person person = mbssUtils.findDetailById(id);
	        if (person == null) {
	            log.info("Person not found");
	            return new ResponseEntity<>("", HttpStatus.NOT_FOUND);
	        }
	        FingerprintTemplates templates = person.getRegistration().get(0).getFingerprintSample().get(0).getTemplates();
	       
	        //Left hand
	        if (templates.getLeftLittle() != null) {
	            templateData.add(Base64Utils.encodeToString(templates.getLeftLittle().getBuffer()));
	        } else {
	            log.warn("No data from leftLittle for calculate hash");
	        }
	        if (templates.getLeftIndex() != null) {
	            templateData.add(Base64Utils.encodeToString(templates.getLeftIndex().getBuffer()));
	        } else {
	            log.warn("No data from leftIndex for calculate hash");
	        }
	        if (templates.getLeftMiddle() != null) {
	            templateData.add(Base64Utils.encodeToString(templates.getLeftMiddle().getBuffer()));
	        } else {
	            log.warn("No data from leftMiddle for calculate hash");
	        }
	        if (templates.getLeftRing() != null) {
	            templateData.add(Base64Utils.encodeToString(templates.getLeftRing().getBuffer()));
	        } else {
	            log.warn("No data from leftRing for calculate hash");
	        }
	        if (templates.getLeftThumb() != null) {
	            templateData.add(Base64Utils.encodeToString(templates.getLeftThumb().getBuffer()));
	        } else {
	            log.warn("No data from leftThumb for calculate hash");
	        }
	        //Right hand
	        if (templates.getRightLittle() != null) {
	            templateData.add(Base64Utils.encodeToString(templates.getRightLittle().getBuffer()));
	        } else {
	            log.warn("No data from rightLittle for calculate hash");
	        }
	        if (templates.getRightIndex() != null) {
	            templateData.add(Base64Utils.encodeToString(templates.getRightIndex().getBuffer()));
	        } else {
	            log.warn("No data from rightIndex for calculate hash");
	        }
	        if (templates.getRightMiddle() != null) {
	            templateData.add(Base64Utils.encodeToString(templates.getRightMiddle().getBuffer()));
	        } else {
	            log.warn("No data from rightMiddle for calculate hash");
	        }
	        if (templates.getRightRing() != null) {
	            templateData.add(Base64Utils.encodeToString(templates.getRightRing().getBuffer()));
	        } else {
	            log.warn("No data from rightRing for calculate hash");
	        }
	        if (templates.getRightThumb() != null) {
	            templateData.add(Base64Utils.encodeToString(templates.getRightThumb().getBuffer()));
	        } else {
	            log.warn("No data from rightThumb for calculate hash");
	        }
        }
        StringBuilder stringBuilder = new StringBuilder();
        templateData.forEach(s -> stringBuilder.append(s));
        String hashTemplate = Hashing.sha256().hashString(stringBuilder.toString(), StandardCharsets.UTF_8).toString();
        String md5 = Hashing.md5().hashString(hashTemplate, StandardCharsets.UTF_8).toString();
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("hashRaw", md5);
        jsonObject.put("hash", String.valueOf(md5.hashCode()));
        return new ResponseEntity<>(jsonObject.toString(), HttpStatus.OK);
    }

    @RequestMapping(value = "/searchDetail/{id}", produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.GET)
    public ResponseEntity<ComplexResponse> findDetailRecord(@PathVariable String id) {
        log.info("lblancas:: [tkn-api-mbss] :: "+this.getClass().getName()+".findDetailRecord ");
        Person person = mbssUtils.findDetailById(id);
        if (person == null) {
            return new ResponseEntity<>((ComplexResponse) null, HttpStatus.NOT_FOUND);
        }
        try {
            ComplexResponse complexResponse = new ComplexResponse();
            complexResponse.setId(id);
            String leftIndex = null;
            String rightIndex = null;
            byte[] leftIndexBuffer = null;
            try {
                leftIndexBuffer = person.getRegistration().get(0).getFingerprintSample().get(0).getTemplates().getLeftIndex().getBuffer();
                if (leftIndexBuffer != null) {
                    leftIndex = Base64Utils.encodeToString(leftIndexBuffer);
                }
                complexResponse.setLeftIndex(leftIndex);
            } catch (NullPointerException e) {
                log.error("No left index found");
            }
            byte[] rightIndexBuffer = null;
            try {
                rightIndexBuffer = person.getRegistration().get(0).getFingerprintSample().get(0).getTemplates().getRightIndex().getBuffer();
                if (rightIndexBuffer != null) {
                    rightIndex = Base64Utils.encodeToString(rightIndexBuffer);
                }
                complexResponse.setRightIndex(rightIndex);
            } catch (Exception e) {
                log.error("No right index found");
            }
            return new ResponseEntity<>(complexResponse, HttpStatus.OK);
        } catch (Exception e) {
            log.error("Error finding complex detail for: {} with message: {}", id, e.getMessage());
            return new ResponseEntity<>((ComplexResponse) null, HttpStatus.UNPROCESSABLE_ENTITY);
        }
    }

    @RequestMapping(value = "/search/{id}", produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.GET)
    public ResponseEntity<BasicResponse> findRecord(@PathVariable String id) 
    {
    	log.info("lblancas:: [tkn-api-mbss] :: "+this.getClass().getName()+".findRecord ");
        HttpStatus response = mbssUtils.findById(id);
        BasicResponse basicResponse = new BasicResponse();
        basicResponse.setId(id);
        basicResponse.setTime(String.valueOf(System.currentTimeMillis() / 1000));
        if (!response.equals(HttpStatus.OK)) {
            basicResponse.setHasFingers(false);
            basicResponse.setHasFacial(false);
        } else {
            basicResponse.setHasFacial(true);
            basicResponse.setHasFingers(true);
        }
        log.info("Returning: {} - {}", response, basicResponse);
        return new ResponseEntity<>(basicResponse, response);
    }

    @RequestMapping(value = "/search/fingers", method = RequestMethod.POST, produces = "application/json", consumes = "application/json")
    public ResponseEntity<BasicResponse> findRecordByFingers(@RequestBody RequestFingers requestFingers){ 
    	HttpStatus statusToAnswer = HttpStatus.NOT_FOUND;
    	log.info("lblancas:: [tkn-api-mbss] :: "+this.getClass().getName()+".findRecordByFingers: ");       
        BasicResponse basicResponse = new BasicResponse();
//    	TODO KARALUNDI
        if(engine.equals(KARALUNDI)) {//TODO         	
			log.info(this.getClass().getName()+".findRecordByFingers: /search/fingers: KARALUNDI");
			basicResponse.setHasFacial(true);
			basicResponse.setHasFingers(true);
			basicResponse.setId(requestFingers.getId());
			statusToAnswer = HttpStatus.OK;
		} else {//MORPHO-->> 
			HttpStatusWrapper responseStatus = mbssUtils.findByFingers(requestFingers.getFingers(), requestFingers.getImageType());
	        log.info("Buscando con datos: {}", requestFingers.getFingers());
			if (responseStatus.getHttpStatus().equals(HttpStatus.CONFLICT)) {
				basicResponse.setHasFacial(true);
				basicResponse.setHasFingers(true);
				basicResponse.setId(responseStatus.getId());
				statusToAnswer = HttpStatus.OK;
			} else if (responseStatus.getHttpStatus().equals(HttpStatus.PRECONDITION_FAILED)) {
				basicResponse.setHasFingers(false);
				basicResponse.setHasFacial(false);
				basicResponse.setId(requestFingers.getId());
				statusToAnswer = HttpStatus.PRECONDITION_FAILED;
			} else {
				basicResponse.setHasFingers(false);
				basicResponse.setHasFacial(false);
				basicResponse.setId(requestFingers.getId());
			}
			log.info("Returning: {} - {}", responseStatus, basicResponse);
		}
		basicResponse.setTime(String.valueOf(System.currentTimeMillis() / 1000));
		return new ResponseEntity<>(basicResponse, statusToAnswer);
    }

    /**
     * Use search fingers with image type instead. Next version of the API will remove this method
     *
     * @param requestFingers
     * @return
     */
    @Deprecated
    @RequestMapping(value = "/search/fingers/jpeg", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = "application/json")
    public ResponseEntity<BasicResponse> findRecordByFingersJPEG(@RequestBody RequestFingers requestFingers) 
    {
    	log.info("lblancas:: [tkn-api-mbss] :: "+this.getClass().getName()+".findRecordByFingersJPEG ");
        HttpStatusWrapper responseStatus = mbssUtils.findByFingersJPEG(requestFingers.getFingers());
        BasicResponse basicResponse = new BasicResponse();
        HttpStatus statusToAnswer = HttpStatus.NOT_FOUND;
        if (responseStatus.getHttpStatus().equals(HttpStatus.CONFLICT)) {
            basicResponse.setHasFacial(true);
            basicResponse.setHasFingers(true);
            basicResponse.setId(responseStatus.getId());
            statusToAnswer = HttpStatus.OK;
        } else if (responseStatus.getHttpStatus().equals(HttpStatus.PRECONDITION_FAILED)) {
            basicResponse.setHasFingers(false);
            basicResponse.setHasFacial(false);
            basicResponse.setId(requestFingers.getId());
            statusToAnswer = HttpStatus.PRECONDITION_FAILED;
        } else {
            basicResponse.setHasFingers(false);
            basicResponse.setHasFacial(false);
            basicResponse.setId(requestFingers.getId());
        }
        basicResponse.setTime(String.valueOf(System.currentTimeMillis() / 1000));
        log.info("Returning: {} - {}", responseStatus, basicResponse);
        return new ResponseEntity<>(basicResponse, statusToAnswer);
    }

    @RequestMapping(value = "/search/fingersById", method = RequestMethod.POST, produces = "application/json", consumes = "application/json")
    public ResponseEntity<BasicResponse> findRecordByFingerAndId(@RequestBody RequestFingers requestFingers){ 
    	log.info("INFO : "+this.getClass().getName()+".findRecordByFingerAndId id: "+requestFingers.getId());
    	BasicResponse basicResponse = new BasicResponse();    	
        HttpStatus httpStatus = HttpStatus.NOT_FOUND;        
    	if(engine.equals(KARALUNDI)) {//TODO		
	        return validateMinuciasByKaralundi(requestFingers);
    	}else {
	    	log.info("lblancas:: [tkn-api-mbss] :: "+this.getClass().getName()+".findRecordByFingerAndId ");
	        HttpStatusWrapper responseStatus = mbssUtils.findByFingersAndId(requestFingers.getFingers(), requestFingers.getId(), requestFingers.getImageType());
	      
	        basicResponse.setId(responseStatus.getId());
	        if (responseStatus.getHttpStatus().equals(HttpStatus.NOT_FOUND)) {
	            basicResponse.setHasFacial(false);
	            basicResponse.setHasFingers(false);
	        } else if (responseStatus.getHttpStatus().equals(HttpStatus.PRECONDITION_FAILED)) {
	            basicResponse.setHasFingers(false);
	            basicResponse.setHasFacial(false);
	            basicResponse.setId(requestFingers.getId());
	            httpStatus = HttpStatus.PRECONDITION_FAILED;
	        } else {
	            httpStatus = HttpStatus.OK;
	            basicResponse.setHasFacial(true);
	            basicResponse.setHasFingers(true);
	        }
	        basicResponse.setTime(String.valueOf(System.currentTimeMillis() / 1000));
	        log.info("Returning: {} - {}", responseStatus, basicResponse);
	        return new ResponseEntity<>(basicResponse, httpStatus);
    	}
    }

    @RequestMapping(value = "/search/slaps", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = "application/json")
    public ResponseEntity<BasicResponse> findRecordByHands(@RequestBody RequestSlaps requestSlaps) 
    {
    	log.info("lblancas:: [tkn-api-mbss] :: "+this.getClass().getName()+".findRecordByHands ");
        HttpStatusWrapper responseStatus = mbssUtils.findBySlaps(requestSlaps.getSlaps());
        BasicResponse basicResponse = new BasicResponse();
        HttpStatus statusToAnswer = HttpStatus.NOT_FOUND;
        if (responseStatus.getHttpStatus().equals(HttpStatus.CONFLICT)) {
            basicResponse.setHasFacial(true);
            basicResponse.setHasFingers(true);
            basicResponse.setId(responseStatus.getId());
            statusToAnswer = HttpStatus.OK;
        } else if (responseStatus.getHttpStatus().equals(HttpStatus.PRECONDITION_FAILED)) {
            basicResponse.setHasFingers(false);
            basicResponse.setHasFacial(false);
            basicResponse.setId(requestSlaps.getId());
            statusToAnswer = HttpStatus.PRECONDITION_FAILED;
        } else {
            basicResponse.setHasFingers(false);
            basicResponse.setHasFacial(false);
            basicResponse.setId(requestSlaps.getId());
        }
        basicResponse.setTime(String.valueOf(System.currentTimeMillis() / 1000));
        log.info("Returning: {} - {}", responseStatus, basicResponse);
        return new ResponseEntity<>(basicResponse, statusToAnswer);
    }

    @RequestMapping(value = "/search/slapsId", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = "application/json")
    public ResponseEntity<BasicResponse> findRecordByHandsAndId(@RequestBody RequestSlaps requestSlaps) 
    {
    	log.info("lblancas:: [tkn-api-mbss] :: "+this.getClass().getName()+".findRecordByHandsAndId ");
        HttpStatusWrapper responseStatus = mbssUtils.findBySlapsAndId(requestSlaps.getId(), requestSlaps.getSlaps());
        BasicResponse basicResponse = new BasicResponse();
        if (responseStatus.equals(HttpStatus.OK)) {
            basicResponse.setHasFacial(true);
            basicResponse.setHasFingers(true);
        } else if (responseStatus.getHttpStatus().equals(HttpStatus.PRECONDITION_FAILED)) {
            basicResponse.setHasFingers(false);
            basicResponse.setHasFacial(false);
            basicResponse.setId(requestSlaps.getId());
        } else {
            basicResponse.setHasFingers(false);
            basicResponse.setHasFacial(false);
        }
        basicResponse.setId(responseStatus.getId());
        basicResponse.setTime(String.valueOf(System.currentTimeMillis() / 1000));
        log.info("Returning: {} - {}", responseStatus, basicResponse);
        return new ResponseEntity<>(basicResponse, responseStatus.getHttpStatus());
    }

    @RequestMapping(value = "/search/facial", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = "application/json")
    public ResponseEntity<BasicResponse> findRecordByFacial(@RequestBody RequestFacial requestFacial) {
    	 log.info("INFO: "+this.getClass().getName()+".findRecordByFacial");
    	  HttpStatus statusToAnswer = HttpStatus.NOT_FOUND;
    	  BasicResponse basicResponse = new BasicResponse();
    	if(engine.equals(KARALUNDI)) {//TODO FACIAL
			return findFacialInKaralundi(requestFacial);			
    	}else {
            log.info("lblancas:: [tkn-api-mbss] :: "+this.getClass().getName()+".findRecordByFacial :  "+requestFacial.getId());
	        HttpStatusWrapper responseStatus = mbssUtils.findByFace(requestFacial.getFacial());
	        log.info("lblancas:: [tkn-api-mbss] :: 1 :"+responseStatus);
	        log.info("lblancas:: [tkn-api-mbss] :: 2");
	        log.info("lblancas:: [tkn-api-mbss] :: 3");
	        if (responseStatus.getHttpStatus().equals(HttpStatus.CONFLICT)) 
	        {
	        	log.info("lblancas:: [tkn-api-mbss] :: 4");
	            basicResponse.setHasFacial(true);
	            basicResponse.setHasFingers(true);
	            basicResponse.setId(responseStatus.getId());
	            statusToAnswer = HttpStatus.OK; 
	            log.info("lblancas:: [tkn-api-mbss] :: 5");
	        }
	        else //no se encontro
	        {
	        	log.info("lblancas:: [tkn-api-mbss] :: 6");
	            basicResponse.setHasFingers(false);
	            basicResponse.setHasFacial(false);
	            basicResponse.setId(requestFacial.getId());
	            log.info("lblancas:: [tkn-api-mbss] :: 7");
	        }
	        basicResponse.setTime(String.valueOf(System.currentTimeMillis() / 1000));
	        log.info("Returning: {} - {}", responseStatus, basicResponse);
	        return new ResponseEntity<>(basicResponse, statusToAnswer);
    	}
    }

    @RequestMapping(value = "/search/facialId", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = "application/json")
    public ResponseEntity<BasicResponse> findRecordByFacialId(@RequestBody RequestFacial requestFacial) {
        log.info("lblancas:: [tkn-api-mbss] :: "+this.getClass().getName()+".findRecordByFacialId ");
//        if(engine.equals(KARALUNDI)) {
//        	ResponseEntity<OperationResult> res = 
//        			facialController.sendVerifyMF(requestFacial.getId(),  Base64Utils.decodeFromString(requestFacial.getFacial()));//TODO flujos2
//        	 BasicResponse basicResponse = new BasicResponse();
//             if (res.getStatusCode().equals(HttpStatus.OK)) {
//                 basicResponse.setHasFacial(true);
//                 basicResponse.setHasFingers(true);
//             } else {
//                 basicResponse.setHasFingers(false);
//                 basicResponse.setHasFacial(false);
//             }
//             basicResponse.setTime(String.valueOf(System.currentTimeMillis() / 1000));
//             log.info("Returning: {} - {}", res, basicResponse);
//             return new ResponseEntity<>(basicResponse, responseStatus.getHttpStatus());
//        }
        HttpStatusWrapper responseStatus = mbssUtils.findByFaceId(requestFacial.getId(), requestFacial.getFacial());
        BasicResponse basicResponse = new BasicResponse();
        if (responseStatus.equals(HttpStatus.OK)) {
            basicResponse.setHasFacial(true);
            basicResponse.setHasFingers(true);
        } else {
            basicResponse.setHasFingers(false);
            basicResponse.setHasFacial(false);
        }
        basicResponse.setId(responseStatus.getId());
        basicResponse.setTime(String.valueOf(System.currentTimeMillis() / 1000));
        log.info("Returning: {} - {}", responseStatus, basicResponse);
        return new ResponseEntity<>(basicResponse, responseStatus.getHttpStatus());
    }

    @RequestMapping(value = "/delete/deleteById", method = RequestMethod.DELETE, produces = "application/json", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<SimpleResponse> deleteById(@RequestBody String jsonRequest) {
        log.info("lblancas:: [tkn-api-mbss] :: "+this.getClass().getName()+".deleteById ");
        JSONObject jsonInput = null;
        try {
            jsonInput = new JSONObject(jsonRequest);
        } catch (Exception e) {
            SimpleResponse sr = new SimpleResponse();
            sr.setStatus("NO_JSON_INPUT");
            sr.setId("0");
            sr.setTime("0");
            return new ResponseEntity<>(sr, HttpStatus.BAD_REQUEST);
        }
        String id = jsonInput.getString("id");
        Person person = mbssUtils.findDetailById(id);
        if (person == null) {
            return new ResponseEntity<>((SimpleResponse) null, HttpStatus.NOT_FOUND);
        }
        try {
            HttpStatus responseStatus = mbssUtils.deleteById(id);
            SimpleResponse response = new SimpleResponse();
            response.setId(id);
            response.setStatus(responseStatus.toString());
            response.setTime(String.valueOf(System.currentTimeMillis() / 1000));

            return new ResponseEntity<>(response, responseStatus);

        } catch (Exception e) {
            return new ResponseEntity<>((SimpleResponse) null, HttpStatus.UNPROCESSABLE_ENTITY);
        }
    }

    @RequestMapping(value = "/compareFacial", method = RequestMethod.POST)
    public ResponseEntity<SimpleResponse> matchFacial(@RequestBody RequestAuthenticateFacial requestAuthenticateFacial) {
        log.info("lblancas:: [tkn-api-mbss] :: "+this.getClass().getName()+".matchFacial ");
        HttpStatusImageWrapper imageWrapper = mbssUtils.compareByPhotos(requestAuthenticateFacial.getFacialB641(), requestAuthenticateFacial.getFacialB642(), requestAuthenticateFacial.getScore(), "tkn_auth_s");
        log.debug("Get from mbss: {}", imageWrapper);
        if (imageWrapper.getHttpStatus().equals(HttpStatus.OK)) {
            return new ResponseEntity<>(new SimpleResponse("1", imageWrapper.getScore(), String.valueOf(System.currentTimeMillis())), HttpStatus.OK);
        }
        return new ResponseEntity<SimpleResponse>(new SimpleResponse("0", "0", String.valueOf(System.currentTimeMillis())), HttpStatus.NOT_FOUND);
    }

    private SimpleResponse buildResponseMissingFields() 
    {
    	log.info("lblancas:: [tkn-api-mbss] :: "+this.getClass().getName()+".buildResponseMissingFields ");
        return new SimpleResponse("400", "400", String.valueOf(System.currentTimeMillis()));
    }

    @SuppressWarnings("unused")
	private SimpleResponse buildResponseBadEncodingFields() 
    {
    	log.info("lblancas:: [tkn-api-mbss] :: "+this.getClass().getName()+".buildResponseBadEncodingFields ");
        return new SimpleResponse("400", "Bad encoding", String.valueOf(System.currentTimeMillis()));
    }
    
    
    
    //----------------------------------------------------KARALUNDI METHODS--------------------------------------------------->>>
    /**
     * Metodo que valida y agregar las minucias al motor de Karalundi.
     * @category DEMO
     * @param requestCreate
     * @return
     */
	private HttpStatus addMinuciasToKaralundi(RequestCreate requestCreate) {
		HttpStatus responseStatus;
		log.info("BiometricEngine: " + engine+ " INETYPE: "+requestCreate.getIneType());

        if(requestCreate.getIneType() != null){
            	ResponseEntity<OperationResult> resp = fingerController.sendEnrollMF(requestCreate.getIneType(),requestCreate.getId(), requestCreate.getFingers());
			log.info("KaralundiResponse:" + resp.getBody().getMessage());
			responseStatus = resp.getStatusCode();
			if (resp.getStatusCode().equals(HttpStatus.UNPROCESSABLE_ENTITY)) {
				responseStatus = HttpStatus.PRECONDITION_FAILED;
			}
			if (resp.getStatusCode().equals(HttpStatus.BAD_REQUEST)) {
				responseStatus = HttpStatus.BAD_REQUEST;
			}
            	return responseStatus;
        }
		
			if (fingerController.sendIdentifyMf(requestCreate.getId(), requestCreate.getFingers()).getStatusCode()
				.equals(HttpStatus.FOUND) ) {
			responseStatus = HttpStatus.PRECONDITION_FAILED;
		}else {
			ResponseEntity<OperationResult> resp = fingerController.sendEnrollMF(requestCreate.getIneType(),requestCreate.getId(), requestCreate.getFingers());
			log.info("KaralundiResponse:" + resp.getBody().getMessage());
			responseStatus = resp.getStatusCode();
			if (resp.getStatusCode().equals(HttpStatus.UNPROCESSABLE_ENTITY)) {
				responseStatus = HttpStatus.PRECONDITION_FAILED;
			}
			if (resp.getStatusCode().equals(HttpStatus.BAD_REQUEST)) {
				responseStatus = HttpStatus.BAD_REQUEST;
			}
		}
		return responseStatus;
	}
	
	/**
	 * Metodo para la validacion previo a firma de contrato DEMO
	 * @param requestFingers
	 * @return
	 */
	private ResponseEntity<BasicResponse> validateMinuciasByKaralundi(RequestFingers requestFingers) {
		HttpStatus httpStatus;
		ResponseEntity<OperationResult> respValidation = fingerController.sendVerifyMF(requestFingers.getId(),requestFingers.getFingers());
		
		BasicResponse basicResponse = new BasicResponse();  
		if (respValidation.getStatusCode().equals(HttpStatus.FOUND)) {
			basicResponse.setHasFacial(true);
			basicResponse.setHasFingers(true);
			httpStatus = HttpStatus.OK;
		} else if (respValidation.getStatusCode().equals(HttpStatus.NOT_FOUND)) {
			basicResponse.setHasFacial(false);
			basicResponse.setHasFingers(false);
			httpStatus = HttpStatus.NOT_FOUND;
		} else {
			basicResponse.setHasFingers(false);
            basicResponse.setHasFacial(false);
            basicResponse.setId(requestFingers.getId());
            httpStatus = HttpStatus.PRECONDITION_FAILED;
		}
        return new ResponseEntity<>(basicResponse, httpStatus);
	}
	
	/**
	 * Metodo para validacion y enrollamiento facial
	 * @param requestFacial
	 * @return
	 */
	private ResponseEntity<BasicResponse> findFacialInKaralundi(RequestFacial requestFacial) {
		//TODO FACIAL
		//TODO WARN!!: SOLO PARA DEMOS; DUMMY LIST---->>
		log.info("INFO: id -->> "+requestFacial.getId());
//		requestFacial.setSpeakers(
//				requestFacial.getSpeakers().isEmpty() ? dummySpeakerSearch(Integer.parseInt(requestFacial.getId().isEmpty()?"99":requestFacial.getId()))
//						: requestFacial.getSpeakers());	
		HttpStatus resp;
		if(!requestFacial.getSpeakers().isEmpty()&&requestFacial.getSpeakers().size() == 2) {
			listCurps = requestFacial.getSpeakers();
			isAutoEnroll = true;
			resp = HttpStatus.FOUND;
		}
		resp = HttpStatus.NOT_FOUND;
		//DUMMY LIST----<<
		BasicResponse basicResponse = new BasicResponse();
		if(facialIdentify) {//variable para activar la verificacioon 
			resp = facialController.findOneFacialByImage(requestFacial.getId(),
				   Base64Utils.decodeFromString(requestFacial.getFacial()), requestFacial.getSpeakers()).getStatusCode();
		}
		if (resp.equals(HttpStatus.FOUND)) {
			basicResponse.setHasFacial(true);
			basicResponse.setHasFingers(true);
			basicResponse.setId(Integer.parseInt(requestFacial.getId())+"");
			log.info("Se encontro un registro previo del usuario : " + Integer.parseInt(requestFacial.getId())+"");
			return new ResponseEntity<>(basicResponse, HttpStatus.OK);
		}else if (resp.equals(HttpStatus.BAD_REQUEST)) {
			basicResponse.setHasFacial(true);
			basicResponse.setHasFingers(true);
			basicResponse.setId(Integer.parseInt(requestFacial.getId())+"");
			log.info(" El id de usuario no corresponde con la imagen ingresada  : " + Integer.parseInt(requestFacial.getId())+"");
			return new ResponseEntity<>(basicResponse, HttpStatus.BAD_REQUEST);
		}else {
			log.info("No se encontro un registro previo del usuario : " + Integer.parseInt(requestFacial.getId())+"");
//			facialController.sendEnrollMF(requestFacial.getId(),
//					Base64Utils.decodeFromString(requestFacial.getFacial()));
			basicResponse.setHasFingers(false);
			basicResponse.setHasFacial(false);
			basicResponse.setId(Integer.parseInt(requestFacial.getId())+"");
			return new ResponseEntity<>(basicResponse, HttpStatus.NOT_FOUND); // no se encontro.
		}
		
//		BasicResponse basicResponse = new BasicResponse();
//		if (facialController.sendIdentifyMF(requestFacial.getId(), Base64Utils.decodeFromString(requestFacial.getFacial())).getStatusCode().equals(HttpStatus.OK)) {
//			basicResponse.setHasFacial(true);
//            basicResponse.setHasFingers(true);
//            basicResponse.setId(requestFacial.getId());
//			log.info("Se encontro un registro previo del usuario : " + requestFacial.getId());
//			return new ResponseEntity<>(basicResponse, HttpStatus.OK);
//		} else {
//			facialController.sendEnrollMF(requestFacial.getId(),Base64Utils.decodeFromString(requestFacial.getFacial()));
//			log.info("Se encontro un registro previo del usuario : " + requestFacial.getId());
//			basicResponse.setHasFingers(false);
//            basicResponse.setHasFacial(false);
//            basicResponse.setId(requestFacial.getId());
//			return new ResponseEntity<>(basicResponse, HttpStatus.NOT_FOUND); //no se encontro.
//		}
		
	}
	
	
	/**
	 * Metodo que agrega facial y huellas si es que no se encuentran registrados.
	 * @param requestCreate
	 * @return
	 */
	private HttpStatus addMinuciasAndFaceToKaralundi(RequestCreate requestCreate) {
		log.info("INFO: "+this.getClass().getName()+".addMinuciasAndFaceToKaralundi: ");
		// Verificar existencia facial.
//		if (facialController.findOneFacialByImage(requestCreate.getId(), Base64Utils.decodeFromString(requestCreate.getFacial()),
//						dummySpeakerSearch(Integer.parseInt(requestCreate.getId())))
//				.getStatusCode().equals(HttpStatus.NOT_FOUND)||isAutoEnroll) {
			// Verificar existencia huellas.
		
//			if(failCaseSelector(requestCreate.getFingers(), requestCreate.getId(),HttpStatus.NOT_FOUND)||
//					verifyUserAutoEnroll(requestCreate.getId(), requestCreate.getFingers())) {
//				
//				if(facialController.sendEnrollMF(requestCreate.getId(), Base64Utils.decodeFromString(
//						requestCreate.getFacial())).getStatusCode().equals(HttpStatus.OK)) {
//					// Enrollar huellas
//					if(fingerController.sendEnrollMF(requestCreate.getId(),  requestCreate.getFingers()).getStatusCode().equals(HttpStatus.OK)){
//						return HttpStatus.OK;
//					}else {
//						log.info(" Error en enrolamiento Fingers.");
//					}					
//				}else {
//					log.info(" Error en enrolamiento Facial.");
//				}
//			}else {
//				log.info(" Fingers duplicados.");
//			}		
//		}	else {			
//			log.info(" Facial duplicado.");
//		}
//		return HttpStatus.CONFLICT;
		
		
		if(fingerController.sendIdentifyMf(requestCreate.getId(), requestCreate.getFingers()).getStatusCode().equals(HttpStatus.NOT_FOUND)||verifyUserAutoEnroll(requestCreate.getId(), requestCreate.getFingers())) {			
			if(facialEnroll) {//ENROLLAMIENTO FACIAL
				facialController.sendEnrollMF(requestCreate.getId(), Base64Utils.decodeFromString(requestCreate.getFacial())).getStatusCode().equals(HttpStatus.OK);
			}
			
			if(fingerController.sendEnrollMF(requestCreate.getIneType(),requestCreate.getId(),  requestCreate.getFingers()).getStatusCode().equals(HttpStatus.OK)){
				return HttpStatus.OK;
			}else {
				log.info(" Error en enrolamiento Fingers.");
			}				
		}else {
			log.info(" Fingers duplicados.");
		}
		return HttpStatus.CONFLICT;		
		
	}
	
	
	private Boolean verifyUserAutoEnroll(String id, Fingers fingers) {
//		obtener lista de id de curp 
		if (isAutoEnroll && listCurps != null && !listCurps.isEmpty()) {
//		obtener el id diferente al de la operacion 
			listCurps.remove(id);
			String idRegistrado = listCurps.get(0);
//		validar que las huellas ingresadas sean iguales a las registradas 
			ResponseEntity<OperationResult> res = fingerController.sendVerifyMF(idRegistrado, fingers);
//		sisi retornar true sino false 
			return res.getStatusCode().equals(HttpStatus.FOUND);
		}
		return false;
	}
	
	
	//ONLY FOR INTERNAL FUNCTIONS------------------------------------------------------------------------------------------------>>>>>
	/**
	 *  metodo para carga masiva al motor morpho
	 */	
//    @RequestMapping(value = "/cargaMasivaMorpho", method = RequestMethod.GET)
//    public void cargaMasivaMorpho() {
//        log.info("lblancas:: [tkn-api-mbss] :: "+this.getClass().getName()+".findDetailRecord ");
//		String lista = "D:\\cargaMorpho\\idUsuarios.json";
//		String carpeta = "D:\\cargaMorpho\\wsqFiles\\";
//		String extencion = ".wsq";
//		FileReader paramsJson;
//				
//		try {
//			//obteniendo la lista de ids a enrollar
//			paramsJson = new FileReader(lista);
//			JsonObject data = (JsonObject) new JsonParser().parse(paramsJson);
//			JsonArray ids = data.getAsJsonArray("ids");
//			// enrrollando lista de ids
//			for (JsonElement op : ids) {				
//				String id = op.toString();
//				Fingers fingers = new Fingers();
//				carpeta = carpeta + id +"\\";
//				System.out.println("Procesando id:"+id+" ....");
//				fingers.setLi(fileToBase64(new File(carpeta+"Minucia_li"+extencion)));		
//				fingers.setLl(fileToBase64(new File(carpeta+"Minucia_ll"+extencion)));		
//				fingers.setLm(fileToBase64(new File(carpeta+"Minucia_lm"+extencion)));		
//				fingers.setLr(fileToBase64(new File(carpeta+"Minucia_lr"+extencion)));		
//				fingers.setLt(fileToBase64(new File(carpeta+"Minucia_lt"+extencion)));			
//				fingers.setRi(fileToBase64(new File(carpeta+"Minucia_ri"+extencion)));		
//				fingers.setRl(fileToBase64(new File(carpeta+"Minucia_rl"+extencion)));		
//				fingers.setRm(fileToBase64(new File(carpeta+"Minucia_rm"+extencion)));		
//				fingers.setRr(fileToBase64(new File(carpeta+"Minucia_rr"+extencion)));		
//				fingers.setRt(fileToBase64(new File(carpeta+"Minucia_rt"+extencion)));
//				
//				String facial = fileToBase64(new File(carpeta+"Rostro-"+id+".jpeg"));
//				checkAndInsertPersonFromFaceAndFingers(id, facial, fingers, "WSQ");	
//				System.out.println("Procesado.");
//				
//			}
//		} catch (FileNotFoundException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//
//	}
    
    private HttpStatus checkAndInsertPersonFromFaceAndFingers(String id, String face, Fingers fingers, String imageType) {
    	log.info("lblancas:: [tkn-api-mbss] :: "+this.getClass().getName()+".checkAndInsertPersonFromFaceAndFingers id:"+id + "  , imageType:"+imageType);
        Person personNotEncodedYetOnlyFingers = buildFromFingers(id, fingers, imageType);
        HttpStatus httpStatus = requestToCompare(personNotEncodedYetOnlyFingers);
        log.info("HttpStatus from onlyFingers: {}", httpStatus);

        if (httpStatus != HttpStatus.OK) {
            return HttpStatus.CONFLICT;
        } else {
            Person candidateToEnroll = buildFromFingers(id, fingers, imageType);
            candidateToEnroll = addFace(candidateToEnroll, face);
            httpStatus = requestToEnroll(candidateToEnroll);
            log.info("Candidate to enroll: {}", httpStatus);
            return httpStatus;
        }
    }
    
    private HttpStatus requestToEnroll(Person personNotEncodedYet) {
    	log.info("lblancas:: [tkn-api-mbss] :: "+this.getClass().getName()+".requestToEnroll(Person personNotEncodedYet) ");
        log.info("Request for enrollment");
        Person encodedPerson = null;
        try {
            encodedPerson = encondePerson(personNotEncodedYet);
        } catch (DuplicateFingerException e) {
            log.error("Duplicate fingers detected: {}", e.getMessage());
            return HttpStatus.PRECONDITION_FAILED;
        }
        log.info("Person encoded ok");
        if (encodedPerson == null) {
            return HttpStatus.BAD_REQUEST;
        }
        //1 to 1 search
        HttpStatusWrapper searchStatus = oneToNSearch(encodedPerson);
        log.info("One to N Status: {}", searchStatus);
        if (!searchStatus.getHttpStatus().equals(HttpStatus.OK)) {
            return searchStatus.getHttpStatus();
        }
        return insertPerson(encodedPerson);
    }
    
    private HttpStatus insertPerson(Person person) {
    	log.info("lblancas:: [tkn-api-mbss] :: "+this.getClass().getName()+".insertPerson(Person person) ");
        log.info("Insert person request...");
        Request insertRequest = WSHelper.insertPersonReq(person);
        Response insertResponse = doRequest(insertRequest);
        log.info("Insert person");
        log.info("Search response: {}", insertResponse);
        log.info("Search error response: {}", insertResponse.getErrorResponse());
        log.info("Search error response basic response: {}", insertResponse.getInsertPersonResponse().getBasicResponse());
        log.info("Search error response basic response error code: {}", insertResponse.getInsertPersonResponse().getBasicResponse().getError());
        if (!insertResponse.getInsertPersonResponse().getBasicResponse().getError().getCode().equals(ErrorCode.SUCCESS)) {
            return HttpStatus.INTERNAL_SERVER_ERROR;
        }
        return HttpStatus.OK;
    }
    
    private HttpStatusWrapper oneToNSearch(Person personEncoded) {
    	log.info("lblancas:: [tkn-api-mbss] :: "+this.getClass().getName()+".oneToNSearch(Person personEncoded) ");
        Request searchReq = WSHelper.searchPPReq(personEncoded);
        Response searchResponse = doRequest(searchReq);
        if (searchResponse.getErrorResponse() == null && searchResponse.getPersonToPersonMatchResponse() != null) {
            if (!searchResponse.getPersonToPersonMatchResponse().getBasicResponse().getError().getCode().equals(ErrorCode.SUCCESS)) {
                return new HttpStatusWrapper(HttpStatus.BAD_REQUEST, null);
            }
            PersonToPersonMatchResponse matchResponse = searchResponse.getPersonToPersonMatchResponse();
            if (!matchResponse.getNoHitRank().equals(0)) {
                String id = matchResponse.getCandidate().get(0).getId();
                return new HttpStatusWrapper(HttpStatus.CONFLICT, id);
            }
            return new HttpStatusWrapper(HttpStatus.OK, null);
        }
        return new HttpStatusWrapper(HttpStatus.BAD_REQUEST, null);
    }
    
    private Person addFace(Person source, String faceB64) {
    	log.info("lblancas:: [tkn-api-mbss] :: "+this.getClass().getName()+".addFace(Person source, String faceB64) ");
        FaceSample sample = new FaceSample();
        StillImage stillImage = new StillImage();
        stillImage.setImage(WSHelper.imageFromByte(Base64Utils.decodeFromString(faceB64), ImageFormatType.JPEG));
        sample.setStillImage(stillImage);
        sample.setSampleType(FaceSampleType.STILL);
        Registration reg = source.getRegistration().get(0);
        reg.getFaceSample().add(sample);
        source.getRegistration().remove(0);
        source.getRegistration().add(reg);
        return source;

    }
    
    
    private HttpStatus requestToCompare(Person personNotEncodedYet) {
    	log.info("lblancas:: [tkn-api-mbss] :: "+this.getClass().getName()+".requestToCompare(Person personNotEncodedYet) ");
        Person encodedPerson = null;
        try {
            encodedPerson = encondePerson(personNotEncodedYet);
        } catch (DuplicateFingerException e) {
            log.error("Duplicate fingers detected: {}", e.getMessage());
            return HttpStatus.PRECONDITION_FAILED;
        }
        log.info("Person encoded ok");
        if (encodedPerson == null) {
            return HttpStatus.BAD_REQUEST;
        }
        //1 to 1 search
        HttpStatusWrapper searchStatus = oneToNSearch(encodedPerson);
        log.info("One to N Status in requestToCompare: {}", searchStatus);
        if (!searchStatus.getHttpStatus().equals(HttpStatus.OK)) {
            return searchStatus.getHttpStatus();
        }
        return searchStatus.getHttpStatus();
    }
    
    private Person encondePerson(Person personToEncode) throws DuplicateFingerException {
    	log.info("lblancas:: [tkn-api-mbss] :: "+this.getClass().getName()+".encondePerson(Person personToEncode) ");
        Request encodeRequest = WSHelper.codePersonReq(personToEncode, true);
        Response encodeResponse = doRequest(encodeRequest);
        try {
            if (!encodeResponse.getEncodePersonResponse().getBasicResponse().getError().getCode().equals(ErrorCode.SUCCESS)) {
                log.info("Error encoding person: {} {} ", encodeResponse.getEncodePersonResponse().getBasicResponse().getError().getCode(), encodeResponse.getEncodePersonResponse().getBasicResponse().getError().getMessage());
                return null;
            }
        } catch (Exception e) {
            log.error("Error getting error code from encondePersonRequest with message: {}", e.getMessage());
            log.error("Error trace: {}", e);
        }
        try {
            //TODO improve if statements
            if (encodeResponse.getEncodePersonResponse().getSequenceCheckErrors().size() == 0) {
                log.info("No sequence check errors found, continue");
            }else if(!encodeResponse.getEncodePersonResponse().getSequenceCheckErrors().isEmpty()){
                boolean isDuplicated = false;
                for (SequenceCheckError seq :
                        encodeResponse.getEncodePersonResponse().getSequenceCheckErrors()) {
                    if(seq.getType().equals(SequenceCheckErrorType.DUPLICATE_ITEM)){
                        isDuplicated = true;
                    }
                }
                if(isDuplicated){
                    JSONArray validatedSequence = validateSecuence(encodeResponse.getEncodePersonResponse());
                    throw new DuplicateFingerException("DUPLICATED_FINGERS", validatedSequence);
                }
            }
        } catch (Exception e) {
            log.error("Error looking for SequenceCheckErrors");
            return null;
        }
        Person encodedPerson = encodeResponse.getEncodePersonResponse().getPerson();
        return encodedPerson;
    }
    
    private JSONArray validateSecuence(EncodePersonResponse response) {
    	log.info("lblancas:: [tkn-api-mbss] :: "+this.getClass().getName()+".validateSecuence(EncodePersonResponse response) ");
        JSONArray results = null;

        List<SequenceCheckError> errors = response.getSequenceCheckErrors();
        log.info("validateSecuence step 1 {}", errors.size());

        if (errors != null && !errors.isEmpty()) {

            log.info("validateSecuence step 2");
            String hand = null;
            String finger = null;
            results = new JSONArray();
            for (SequenceCheckError err : errors) {
                log.info("validateSecuence step 3");
                String value = "";
                for (SequenceCheckErrorItem item : err.getItems()) {
                    log.info("validateSecuence step 4");
                    hand = item.getDecodedPosition().getPrintPosition().getHandPosition().value();
                    finger = item.getDecodedPosition().getPrintPosition().getFingerPosition().value();

                    JSONObject jo = new JSONObject();
                    try {
                        jo.put("hand", hand);
                        jo.put("finger", finger);
                    } catch (JSONException e) {
                        log.error("Error building JSON block for duplicated fingers");
                    }
                    //value = value + "Duplicated in hand: " + hand + " finger: " + finger + " - ";
                    value = "" + jo;
                    //System.out.println("Value: " + value);
                    //results.put(value);
                }
                log.info("Value: {}", value);
                results.put(value);
            }
        }
        return results;
    }
    
    private IWSMbss mbss;//TODO ELPEXE
    public Response doRequest(Request request) {
    	log.info("lblancas:: [tkn-api-mbss] :: "+this.getClass().getName()+".doRequest ");
        Response response = null;
        if (mbss == null) {
            buildPortType();
        }
        if (this.mbss != null) {
            try {
                if (request != null) {                    
                    log.info("lblancas:: [tkn-api-mbss]   "+request.toString());
                    log.info("lblancas:: [tkn-api-mbss]   "+this.routingData);
                	log.info("RequestType"+ request.getRequestType().value());
                    response = this.mbss.process(request, this.routingData);
                }
            } catch (Exception e) {
                log.error("Error requesting data to mbss server: {}", e.getMessage());
            }
        }

        return response;
    }
    private RoutingData routingData;
    private void buildPortType(){
	    	log.info("lblancas:: [tkn-api-mbss] :: "+this.getClass().getName()+".buildPorType http::\\morpho.com+++++++++++++++ ");
	        try {
	            QName qname = new QName("http://www.morpho.com/mbss/generic/wsdl", "MbssService");
	            this.routingData = new RoutingData();
	            this.routingData.setPriority(Integer.valueOf(5));
	            this.routingData.setMatchToPersonSLALevel(Integer.valueOf(0));
	            String  wsdl = "http://189.203.240.16:8080/service/mbss?wsdl";
	            URL url = new URL(wsdl);
	            MbssService service = new MbssService(url, qname);
	            this.mbss = service.getMbssPort();
	        } catch (MalformedURLException e) {
	            log.error("Error building WSDL: {}", e.getMessage());
	        }
    	
    }
    
    
    private Person buildFromFingers(String id, Fingers fingers, String imageType) {
    	log.info("lblancas:: [tkn-api-mbss] :: "+this.getClass().getName()+".buildFromFingers(String id, Fingers fingers, String imageType) ");
        ImageFormatType imageType1 = ImageFormatType.valueOf(imageType.trim().toUpperCase());
        FingersBytes fingersBytes = new FingersBytes(fingers);
        Person personNotEncodedYet = WSHelper.buildPersonFromFingers(id, fingersBytes.getRt(), fingersBytes.getRi(),
                fingersBytes.getRm(), fingersBytes.getRr(), fingersBytes.getRl(), fingersBytes.getLt(),
                fingersBytes.getLi(), fingersBytes.getLm(), fingersBytes.getLr(), fingersBytes.getLl(),
                imageType1, FingerprintSampleType.TENPRINT_SLAP);
        return personNotEncodedYet;
    }
    
	
	private String fileToBase64(File file) {		
		return Base64.getEncoder().encodeToString(fileToByteArray(file));
	}
	
	/**
     * This method uses java.io.FileInputStream to read
     * file content into a byte array
     * @param file
     * @return
     */
    private static byte[] fileToByteArray(File file){
        FileInputStream fis = null;
        // Creating a byte array using the length of the file
        // file.length returns long which is cast to int
        byte[] bArray = new byte[(int) file.length()];
        try{
            fis = new FileInputStream(file);
            fis.read(bArray);
            fis.close();        
            
        }catch(IOException ioExp){
            ioExp.printStackTrace();
        }
        return bArray;
    }
	
	
}
