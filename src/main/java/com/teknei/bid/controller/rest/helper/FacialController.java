package com.teknei.bid.controller.rest.helper;

import java.io.File;
import java.io.IOException;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.teknei.bid.biometric.data.OperationResult;
import com.teknei.bid.biometric.data.karalundi.RequestKaralundi;
import com.teknei.bid.biometric.data.karalundi.ResponseKaralundi;
import com.teknei.bid.biometric.service.KaralundiFaceService;

@Service
public class FacialController {

	/**
	 * Logger
	 */
	private static Logger logger = LoggerFactory.getLogger(FacialController.class);

	@Autowired
	private KaralundiFaceService karalundiFaceService;

	@Value("${tkn.karalundi.resource.face.configuration}")
    private String configuration;
	@Value("${tkn.karalundi.resource.face.enterpriseId}")
    private String enterpriseId;
	@Value("${tkn.karalundi.resource.face.speakerList}")
    private String speakerList;
	
	/**
	 * Verifica existencia de rostro.
	 * 
	 * @param operationId
	 * @param byteImage
	 * @return OK if get one record found, NOT_FOUND and UNPROCESSABLE_ENTITY if fail.
	 */
	public ResponseEntity<OperationResult> sendIdentifyMF(String operationId, byte[] byteImage) {
		logger.info("INFO: " + this.getClass().getName() + ".sendIdentifyMF: operationId: " + operationId + " image.length:"
				+ byteImage.length);
		RequestKaralundi requestParam = new RequestKaralundi();
		try {
			requestParam = startSessionAndGetParams(operationId, byteImage);
			requestParam.setList(speakerList);
			return responseVerifyMF(karalundiFaceService.identifyMF(requestParam), operationId);
		} catch (Exception ex) {
			logger.error("Ocurrió un error al parsear el json", ex);
			OperationResult operationResult = new OperationResult();
			operationResult.setResultOK(false);
			operationResult.setErrorMessage("20001");
			return new ResponseEntity<>(operationResult, HttpStatus.BAD_REQUEST);
		} finally {
			if (!requestParam.getSession().isEmpty()) {
				karalundiFaceService.endSession(requestParam);
			}
		}
	}

	/**
	 * Verifica existencia de rostro.
	 * 
	 * @param operationId
	 * @param byteImage
	 * @return OK if get one record found, NOT_FOUND and BAD_REQUEST|UNPROCESSABLE_ENTITY if fail.
	 */
	public ResponseEntity<OperationResult> sendVerifyMF(String operationId, byte[] byteImage) {
		logger.info("INFO: " + this.getClass().getName() + ".verifyFace: operationId: " + operationId + " image.length:"
				+ byteImage.length);
		RequestKaralundi requestParam = new RequestKaralundi();
		try {
			requestParam = startSessionAndGetParams(operationId, byteImage);
			return responseVerifyMF(karalundiFaceService.verifyMF(requestParam), operationId);
		} catch (Exception ex) {
			logger.error("Ocurrió un error al parsear el json", ex);
			OperationResult operationResult = new OperationResult();
			operationResult.setResultOK(false);
			operationResult.setErrorMessage("20001");
			return new ResponseEntity<>(operationResult, HttpStatus.BAD_REQUEST);
		} finally {
			if (!requestParam.getSession().isEmpty()) {
				karalundiFaceService.endSession(requestParam);
			}
		}
	}

	/**
	 * Agrega una imagen facial.
	 * 
	 * @param operationId
	 * @param byteImage
	 * @return  OK or UNPROCESSABLE_ENTITY|BAD_REQUEST if fail.
	 */
	public ResponseEntity<OperationResult> sendEnrollMF(String operationId, byte[] byteImage) {		
		logger.info("INFO:" + this.getClass().getName() + ".sendEnrollMF: operationId: " + operationId + " image.length:"
				+ byteImage.length);
//		RequestKaralundi requestParam = new RequestKaralundi();   TODO
//		try {
//			requestParam = startSessionAndGetParams(operationId, byteImage);
//			return responseEnrollMF(karalundiFaceService.enrollMF(requestParam));
//		} catch (Exception ex) {
//			logger.error(" Ocurrió un error inesperado:", ex);
//			OperationResult operationResult = new OperationResult();
//			operationResult.setResultOK(false);
//			operationResult.setErrorMessage("20001");
//			return new ResponseEntity<>(operationResult, HttpStatus.BAD_REQUEST);
//		} finally {
//			if (!requestParam.getSession().isEmpty()) {
//				karalundiFaceService.endSession(requestParam);
//			}
//		}
		return new ResponseEntity<>(null, HttpStatus.OK);
	}

	/**
	 * Obtiene y setea parametros de sesion, speaker y file.
	 * 
	 * @param operationId
	 * @param byteImage
	 * @return
	 * @throws IOException
	 */
	private RequestKaralundi startSessionAndGetParams(String operationId, byte[] byteImage) throws IOException {
		RequestKaralundi requestParam = new RequestKaralundi();
		logger.info(" OperationId: " + operationId);
		ResponseKaralundi respKara = karalundiFaceService.startSession(configuration, enterpriseId, "");
		logger.info(" Session: " + respKara.getSession());
		requestParam.setSession(respKara.getSession());
		requestParam.setSpeaker(String.valueOf(operationId));
		requestParam.setFile(byteImageToFile(byteImage, operationId));
		return requestParam;
	}

	/**
	 * Maneja la respuesta.
	 * 
	 * @param respKara
	 * @return OK or UNPROCESSABLE_ENTITY if fail.
	 */
	private ResponseEntity<OperationResult> responseEnrollMF(ResponseKaralundi respKara) {
		logger.info("INFO: "+this.getClass().getName()+".responseEnrollMF:");
		OperationResult operationResult = new OperationResult();
		if (respKara.getStatus() != null && "SUCCESS".equals(respKara.getStatus().toUpperCase())) {
			logger.info(" El enrolamiento fue exitoso");
			operationResult.setResultOK(true);
			operationResult.setMessage("Se ha almacenado el rostro en el expediente");
			return new ResponseEntity<>(operationResult, HttpStatus.OK);
		} else {
			logger.error(" Ocurrió un error al enrolar al usuario");
			operationResult.setResultOK(false);
			operationResult.setErrorMessage("20001");
			return new ResponseEntity<>(operationResult, HttpStatus.UNPROCESSABLE_ENTITY);
		}
	}

	/**
	 * Maneja la respuesta de verificacion
	 * 
	 * @param respKara
	 * @param operationId
	 * @return  OK if get one record found, NOT_FOUND and UNPROCESSABLE_ENTITY if fail.
	 */
	private ResponseEntity<OperationResult> responseVerifyMF(ResponseKaralundi respKara, String operationId) {
		logger.info("INFO: "+this.getClass().getName()+".responseVerifyMF:");
		OperationResult operationResult = new OperationResult();
		String status = respKara.getStatus(); // FAIL, SUCCESS
		logger.info("Status: " + status);
		if (status != null && "SUCCESS".equals(respKara.getStatus().toUpperCase())) {
			if ("ACCEPTED".equals(respKara.getOutcome())) {
				logger.info(" Verificación exitosa usuario: " + operationId);
				return new ResponseEntity<>(operationResult, HttpStatus.OK);
			} else {
				logger.error(" Verificación rechazada usuario: " + operationId);
				operationResult.setResultOK(false);
				operationResult.setErrorMessage("20001");
				return new ResponseEntity<>(operationResult, HttpStatus.NOT_FOUND);
			}
		} else {
			logger.error(" Ocurrió un error al verificar al usuario con OperationId: " + operationId);
			operationResult.setResultOK(false);
			operationResult.setErrorMessage("20001");
			return new ResponseEntity<>(operationResult, HttpStatus.UNPROCESSABLE_ENTITY);
		}
	}
	
	/**
	 * Metodo de validacion facial en koala en base a una lista de operationId enrolados.
	 * @param operationId
	 * @param byteImage
	 * @param speakers
	 * @return FOUND , NOT_FOUND, BAD_REQUEST or INTERNAL_SERVER_ERROR if fail.
	 */
	public ResponseEntity<OperationResult>  findOneFacialByImage(String operationId, byte[] byteImage, List<String> speakers){
		logger.info(" INFO: "+this.getClass().getName()+".findOneFacialByImage:");
		RequestKaralundi requestParam = new RequestKaralundi();	    
		// Abriendo session.
		try {
			ResponseKaralundi respKara = karalundiFaceService.startSession(configuration, enterpriseId, "");
			requestParam.setSession(respKara.getSession());
			requestParam.setFile(byteImageToFile(byteImage, operationId));	
			for(String id : speakers) {
				// Recorriendo lista de ids/speakers...
				logger.info(" Validando Speaker: "+id);
				requestParam.setSpeaker(String.valueOf(id));				
				ResponseKaralundi resp = karalundiFaceService.verifyMF(requestParam);
				if (resp.getStatus() != null && "SUCCESS".equals(resp.getStatus().toUpperCase())) {
					if ("ACCEPTED".equals(resp.getOutcome())) {
						logger.info(" Usuario: "+operationId+", con registro previo operationId: "+ id);
						return new ResponseEntity<>(null, HttpStatus.FOUND);						
					} else if("REJECTED".equals(resp.getOutcome())) {
						logger.info(" El id de usuario no corresponde con la imagen ingresada ");
						if(speakers.size()==2) {// solo para proceso de verificacion de curp 
							return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);	
						}
					} 
				} 
			}	
			return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
		} catch (IOException e) {
			logger.error("ERROR: Error al convertir imagen.",e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}finally {
			if (!requestParam.getSession().isEmpty()) {
				karalundiFaceService.endSession(requestParam);
			}
		}
	}
	

	/**
	 * Clase para convercion de byte [] a File
	 * @param byteImage
	 * @param operationId
	 * @return File
	 * @throws IOException
	 */
	private File byteImageToFile(byte[] byteImage, String operationId) throws IOException {
		File convFile = new File(operationId + ".jpg");
		FileUtils.writeByteArrayToFile(convFile, byteImage);
		return convFile;
	}

}
