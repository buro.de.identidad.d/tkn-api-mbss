package com.teknei.bid.biometric.service.impl;

import java.io.File;
import java.net.Authenticator;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.teknei.bid.biometric.data.karalundi.RequestKaralundi;
import com.teknei.bid.biometric.data.karalundi.ResponseKaralundi;
import com.teknei.bid.biometric.service.InvokerService;
import com.teknei.bid.biometric.service.KaralundiFingerService;
import com.teknei.bid.biometric.util.JSONParseUtil;
import com.teknei.bid.biometric.util.MultipartUtility;
import com.teknei.bid.karalundi.wsdl.AddSpeakersToListRes;
import com.teknei.bid.karalundi.wsdl.Administration;
import com.teknei.bid.karalundi.wsdl.AdministrationService;
import com.teknei.bid.karalundi.wsdl.AuthenticationException;
import com.teknei.bid.service.CustomAuthenticator;

@Service
public class KaralundiFingerServiceImpl implements KaralundiFingerService {

	/**
	 * Logger
	 */
	private static Logger logger = LoggerFactory.getLogger(KaralundiFingerServiceImpl.class); 

	@Value("${tkn.karalundi.host}")
	private String hostFinger;
	
	@Value("${tkn.karalundi.resource.finger.startSesion}")
	private String rscFingerStartSession;
	@Value("${tkn.karalundi.resource.finger.enrollment}")
	private String rscFingerEnrolment;
	@Value("${tkn.karalundi.resource.finger.disenroll}")
	private String rscFingerDisenroll;
	@Value("${tkn.karalundi.resource.finger.verifyMF}")
	private String rscFingerVerify;
	@Value("${tkn.karalundi.resource.finger.isTrained}")
	private String rscFingerIsTrained;
	@Value("${tkn.karalundi.resource.finger.endSession}")
	private String rscFingerEndSession;
	@Value("${tkn.karalundi.resource.finger.deleteSpareSegments}")
	private String rscFingerDeleteSpareSegments;
	@Value("${tkn.karalundi.resource.finger.identity}")
	private String rscFingerIdentify;
	@Value("${tkn.karalundi.resource.finger.update}")
	private String rscFingerUpdate;
	@Value("${tkn.karalundi.resource.finger.speackerList}")
	private String speackerList;
	

	@Autowired
	private InvokerService invokerService;
	
	@Override
	public ResponseKaralundi startSession(String configuration, String enterpriseId, String device) {
		Map<String,Object> params = new LinkedHashMap<>();
		params.put("configuration", configuration);
		params.put("enterpriseId", enterpriseId);
		return requestKaralundi(hostFinger + rscFingerStartSession, params);
	}

	@Override
	public ResponseKaralundi endSession(RequestKaralundi request) {
		Map<String,Object> params = new LinkedHashMap<>();
		params.put("session", request.getSession());
		return requestKaralundi(hostFinger + rscFingerEndSession, params);
	}

	@Override
	public ResponseKaralundi isTrained(RequestKaralundi request) {
		Map<String,Object> params = new LinkedHashMap<>();
		params.put("session", request.getSession());
		params.put("speaker", request.getSpeaker());
		return requestKaralundi(hostFinger + rscFingerIsTrained, params);
	}

	@Override
	public ResponseKaralundi enrollMF(RequestKaralundi request) {
		Map<String,Object> params = new LinkedHashMap<>();
		params.put("session", request.getSession());
		params.put("speaker", request.getSpeaker());
		params.put("data", request.getData());
		return requestMultipartKaralundi(hostFinger + rscFingerEnrolment, params, request.getFile());
	}

	@Override
	public ResponseKaralundi disenroll(RequestKaralundi request) {
		Map<String,Object> params = new LinkedHashMap<>();
		params.put("session", request.getSession());
		params.put("speaker", request.getSpeaker());
		params.put("image", request.getImage());
		return requestKaralundi(hostFinger + rscFingerDisenroll, params);
	}

	@Override
	public ResponseKaralundi deleteSegments(RequestKaralundi request) {
		Map<String,Object> params = new LinkedHashMap<>();
		params.put("session", request.getSession());
		params.put("speaker", request.getSpeaker());
		params.put("image", request.getImage());
		return requestKaralundi(hostFinger + rscFingerDeleteSpareSegments, params);
	}

	@Override
	public ResponseKaralundi verifyMF(RequestKaralundi request) {
		Map<String,Object> params = new LinkedHashMap<>();
		params.put("session", request.getSession());
		params.put("speaker", request.getSpeaker());
		logger.info(" >> speaker:"+request.getSpeaker());
		if(request.getMapFiles().size() == 1) {
			Map<String,File> newMap = new HashMap<String, File>();
			for(Map.Entry<String,File> entry: request.getMapFiles().entrySet()){
				logger.info("Info: Received : "+entry.getKey()+" Forced to : left-index");
				newMap.put("left-thumb", entry.getValue());
				newMap.put("left-index", entry.getValue());
				newMap.put("left-middle", entry.getValue());
				newMap.put("left-ring", entry.getValue());
				newMap.put("left-little", entry.getValue());
				newMap.put("right-thumb", entry.getValue());
				newMap.put("right-index", entry.getValue());
				newMap.put("right-middle", entry.getValue());
				newMap.put("right-ring", entry.getValue());
				newMap.put("right-little", entry.getValue());
			}
			return requestMultipartKaralundi(hostFinger + rscFingerVerify, params, newMap);
		}else {
			return requestMultipartKaralundi(hostFinger + rscFingerVerify, params, request.getMapFiles());
		}
	}
	


	@Override
	public ResponseKaralundi identifyMF(RequestKaralundi request) {
		Map<String, Object> params = new LinkedHashMap<>();
		params.put("session", request.getSession());
		params.put("speaker", request.getSpeaker());
//		if (request.getMapFiles().size() < 10) {
			Map<String, File> newMap = new HashMap<String, File>();
			for (Map.Entry<String, File> entry : request.getMapFiles().entrySet()) {
				logger.info("Buscando por:" + entry.getKey());
				newMap.put("left-thumb", entry.getValue());
				newMap.put("left-index", entry.getValue());
				newMap.put("left-middle", entry.getValue());
				newMap.put("left-ring", entry.getValue());
				newMap.put("left-little", entry.getValue());
				newMap.put("right-thumb", entry.getValue());
				newMap.put("right-index", entry.getValue());
				newMap.put("right-middle", entry.getValue());
				newMap.put("right-ring", entry.getValue());
				newMap.put("right-little", entry.getValue());
				break;
			}
//		  ResponseKaralundi res = requestMultipartKaralundi(hostFinger + rscFingerIdentify, params, newMap);
		  ResponseKaralundi res = requestMultipartKaralundi(hostFinger + rscFingerIdentify, params, request.getMapFiles());
		  String r = new Gson().toJson(res);
		  if(r.contains("error 1115")) {
			  logger.error("ERROR:1115");
			  res = requestMultipartKaralundi(hostFinger + rscFingerIdentify, params, newMap);
		  }
		  
		  return res;		 
//		} else {
//			return requestMultipartKaralundi(hostFinger + rscFingerIdentify, params, request.getMapFiles());
//		}
	}

//	@Override
//	public ResponseKaralundi updateMF(RequestKaralundi request) {
//		Map<String,Object> params = new LinkedHashMap<>();
//		params.put("session", request.getSession());
//		params.put("speaker", request.getSpeaker());
//		params.put("param", request.getParam());
//
//		params.put("left-index", request.getLeftIndex());
//		params.put("left-middle", request.getLeftMiddle());
//		params.put("left-ring", request.getLeftRing());
//		params.put("left-little", request.getLeftLittle());
//		params.put("left-thumb", request.getLeftThumb());
//		
//		params.put("right-index", request.getRightIndex());
//		params.put("right-middle", request.getRightMiddle());
//		params.put("right-ring", request.getRightRing());
//		params.put("right-little", request.getRightLittle());
//		params.put("right-thumb", request.getRightThumb());
//		
//		return requestKaralundi(hostFinger + rscFingerUpdate, params);
//	}
	
	@Override
	public ResponseKaralundi updateMF(RequestKaralundi request) {
		Map<String, Object> params = new LinkedHashMap<>();
		params.put("session", request.getSession());
		params.put("speaker", request.getSpeaker());
		params.put("param", request.getParam());
		return requestMultipartKaralundi(hostFinger + rscFingerUpdate, params, request.getMapFiles());
	}

	private ResponseKaralundi requestKaralundi(String url, Map<String,Object> params) {
		String response = invokerService.send(url, params);
		ResponseKaralundi responseKaralundi = null;
		if(response != null)
			responseKaralundi = JSONParseUtil.stringToResponseKaralundi(response);
		return responseKaralundi;
	}
	
	private ResponseKaralundi requestMultipartKaralundi(String url, Map<String,Object> params, File file) {
		ResponseKaralundi responseKaralundi = null;

		String response = null;
		try {
			MultipartUtility multipart = new MultipartUtility(url);
			multipart.addFormFieldMap(params);
			
//			boolean contains = file.getName().contains("WSQ");
			
			multipart.addFilePart("file", file);
			
			List<String> lstResponse = multipart.finish();
			if(lstResponse != null && !lstResponse.isEmpty()) {
				response = lstResponse.get(0);
				logger.info(" >>> Size Response: " + lstResponse.size());
				logger.info(" >>> RESPONSE " + response);
			}
			
			//response = lstResponse != null ? lstResponse.get(0) : null;
		} catch (Exception ex) {
			logger.error("Ocurrió un error al realizar la petición al servicio.", ex);
		}
		
		if(response != null)
			responseKaralundi = JSONParseUtil.stringToResponseKaralundi(response);
		return responseKaralundi;
	}
	
	private ResponseKaralundi requestMultipartKaralundi(String url, Map<String,Object> params, Map<String,File> files) {

		logger.info(" >>> Url: " + url);
		ResponseKaralundi responseKaralundi = null;

		String response = null;
		try {
			MultipartUtility multipart = new MultipartUtility(url);
			multipart.addFormFieldMap(params);
			
			for(Map.Entry<String,File> entry: files.entrySet()){				
				logger.info(" >>> Param: "+entry.getKey()+";"+entry.getValue().getName());			
				multipart.addFilePart(entry.getKey(),entry.getValue());				
			}
			
			List<String> lstResponse = multipart.finish();
			if(lstResponse != null && !lstResponse.isEmpty()) {
				response = lstResponse.get(0);
				logger.info(" >>> Size Response: " + lstResponse.size());
				logger.info(" >>> RESPONSE " + response);
			}			
			
			for(Map.Entry<String,File> entry: files.entrySet()){
				entry.getValue().delete();
			}
			//response = lstResponse != null ? lstResponse.get(0) : null;
		} catch (Exception ex) {
			logger.error("Ocurrió un error al realizar la petición al servicio.", ex);
		}
		
		if(response != null)
			responseKaralundi = JSONParseUtil.stringToResponseKaralundi(response);
		return responseKaralundi;
	}
	
	
	@Override
	public String addSpeakersToList(RequestKaralundi request) {
		String status = null; 
		List<String> speakers = new ArrayList<String>();
		speakers.add(request.getSpeaker());
		logger.info(">>>addSpeakersToList: {}", speackerList);
		try {
			Authenticator.setDefault(new CustomAuthenticator("manager", "manager123"));
			Administration admin = new Administration();
			AdministrationService adminService = admin.getAdministrationServicePort();
			AddSpeakersToListRes response = adminService.addSpeakersToList(request.getSession(), speackerList,
					speakers);
			status = response.getStatus().value();
			Gson gson = new Gson();
			logger.info("INFO: addSpeakersToList.Response->> "+gson.toJson(response));
		} catch (AuthenticationException ex) {
			logger.error(">>>AuthenticationException Error:" + ex.getMessage());
			ex.printStackTrace();
		} catch (Exception e2) {
			logger.error(">>>GeneralException Error:" + e2.getMessage());
			e2.printStackTrace();
		}
		return status;
	}

	public String addSpeakersToListIneValidation(RequestKaralundi request, String speakerList) {
		String status = null; 
		List<String> speakers = new ArrayList<String>();
		speakers.add(request.getSpeaker());
		String speakerListIne = speakerList;//"ListaHuellasDactilares_SBL_INE_1";
		logger.info(">>>addSpeakersToList: {}", speakerListIne);
		try {
			Authenticator.setDefault(new CustomAuthenticator("manager", "manager123"));
			Administration admin = new Administration();
			AdministrationService adminService = admin.getAdministrationServicePort();
			AddSpeakersToListRes response = adminService.addSpeakersToList(request.getSession(), speakerListIne,
					speakers);
			status = response.getStatus().value();
			Gson gson = new Gson();
			logger.info("INFO: addSpeakersToList.Response->> "+gson.toJson(response));
		} catch (AuthenticationException ex) {
			logger.error(">>>AuthenticationException Error:" + ex.getMessage());
			ex.printStackTrace();
		} catch (Exception e2) {
			logger.error(">>>GeneralException Error:" + e2.getMessage());
			e2.printStackTrace();
		}
		return status;
	}
	
}
