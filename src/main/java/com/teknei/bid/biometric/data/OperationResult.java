package com.teknei.bid.biometric.data;

import java.io.Serializable;

public class OperationResult implements Serializable {

	/**
	 * Serial Version UID
	 */
	private static final long serialVersionUID = 1L;

	private boolean resultOK;
	private String errorMessage;
	private String message;

	/**
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * @param message the message to set
	 */
	public void setMessage(String message) {
		this.message = message;
	}

	/**
	 * @return the resultOK
	 */
	public boolean isResultOK() {
		return resultOK;
	}

	/**
	 * @param resultOK the resultOK to set
	 */
	public void setResultOK(boolean resultOK) {
		this.resultOK = resultOK;
	}

	/**
	 * @return the errorMessage
	 */
	public String getErrorMessage() {
		return errorMessage;
	}

	/**
	 * @param errorMessage the errorMessage to set
	 */
	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}
}