package com.teknei.bid.dto.response;

import lombok.Data;

import java.io.Serializable;

/**
 * Created by Amaro on 06/07/2017.
 */
@Data
public class BasicResponse implements Serializable {

    private String id;
    private Boolean hasFingers;
    private Boolean hasFacial;
    private String time;

}