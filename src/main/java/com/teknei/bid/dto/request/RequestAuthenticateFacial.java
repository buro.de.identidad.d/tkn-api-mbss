package com.teknei.bid.dto.request;

import lombok.Data;

import java.io.Serializable;

@Data
public class RequestAuthenticateFacial implements Serializable {

    private String facialB641;
    private String facialB642;
    private Integer score;

}