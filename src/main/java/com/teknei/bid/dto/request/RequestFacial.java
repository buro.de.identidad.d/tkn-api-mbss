package com.teknei.bid.dto.request;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Amaro on 06/07/2017.
 */
@Data
public class RequestFacial implements Serializable {
	private static final long serialVersionUID = 1L;
	private String id;
    private String facial;
    private List<String> speakers;

}