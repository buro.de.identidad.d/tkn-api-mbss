package com.teknei.bid.dto.request;

import com.teknei.bid.dto.Fingers;
import com.teknei.bid.dto.Slaps;
import lombok.Data;
import lombok.ToString;

import java.io.Serializable;

/**
 * Created by Amaro on 06/07/2017.
 */
@Data
@ToString(exclude = "{slaps, fingers}")
public class RequestCreate implements Serializable {

    private String id;
    private Fingers fingers;
    private Slaps slaps;
    private String facial;
    private String type;
    private String imageType;
    private Integer ineType;
}
