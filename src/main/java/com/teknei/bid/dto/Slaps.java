package com.teknei.bid.dto;

import lombok.Data;

import java.io.Serializable;

/**
 * Created by Amaro on 06/07/2017.
 */
@Data
public class Slaps implements Serializable {

    private String ls;
    private String rs;
    private String ts;

}
