package com.teknei.bid.ws.util;

import com.morpho.mbss.generic.wsdl.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.xml.namespace.QName;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by Amaro on 07/07/2017.
 */
@Component
public class WSInvoker {

    @Value("${mbss.wsdl}")
    private String wsdl;
    @Value("${mbss.qname.url}")
    private String qnameURL;
    @Value("${mbss.qname.name}")
    private String qnameServiceName;
    private IWSMbss mbss;
    private RoutingData routingData;
    private static final Logger log = LoggerFactory.getLogger(WSInvoker.class);

    
    @Value("${biometric.engine}")
    private String engine;
    private static final String KARALUNDI = "karalundi"; 

    @PostConstruct
    private void postConstruct() {
    	if(!engine.equals(KARALUNDI)) {
//    		wsdl = "http://172.20.132.21:8080/service/mbss?wsdl";//TODO morpho productivo. 
    		wsdl = "http://189.203.240.16:8080/service/mbss?wsdl";//TODO morpho productivo ACCESO LOCAL. 
    		buildPortType();
    	}
    }

    private void buildPortType(){
    	if(!engine.equals(KARALUNDI)) {
	    	log.info("lblancas:: [tkn-api-mbss] :: "+this.getClass().getName()+".buildPorType http::\\morpho.com+++++++++++++++ ");
	        try {
	            QName qname = new QName("http://www.morpho.com/mbss/generic/wsdl", "MbssService");
	            this.routingData = new RoutingData();
	            this.routingData.setPriority(Integer.valueOf(5));
	            this.routingData.setMatchToPersonSLALevel(Integer.valueOf(0));
	            URL url = new URL(wsdl);
	            MbssService service = new MbssService(url, qname);
	            this.mbss = service.getMbssPort();
	        } catch (MalformedURLException e) {
	            log.error("Error building WSDL: {}", e.getMessage());
	        }
    	}
    }

    public Response doRequest(Request request) {
    	log.info("lblancas:: [tkn-api-mbss] :: "+this.getClass().getName()+".doRequest ");
        Response response = null;
        if (this.mbss != null) {
            try {
                if (request != null) {
                    if (mbss == null) {
                        buildPortType();
                    }
                    log.info("lblancas:: [tkn-api-mbss]   "+request.toString());
                    log.info("lblancas:: [tkn-api-mbss]   "+this.routingData);
                	log.info("RequestType"+ request.getRequestType().value());
                    response = this.mbss.process(request, this.routingData);
                }
            } catch (Exception e) {
                log.error("Error requesting data to mbss server: {}", e.getMessage());
            }
        }

        return response;
    }
    
    
}
