package com.teknei.bid.ws.util;

import com.morpho.mbss.generic.wsdl.*;
import com.morpho.util.WSHelper;
import com.teknei.bid.dto.*;
import com.teknei.bid.util.DuplicateFingerException;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.util.Base64Utils;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Amaro on 07/07/2017.
 */
@Component
public class MbssUtils {

    private static final Logger log = LoggerFactory.getLogger(MbssUtils.class);

    @Autowired
    private WSInvoker wsInvoker;

    public HttpStatusImageWrapper compareByPhotos(String b64Photo1, String b64Photo2, int umbral, String id) 
    {
    	log.info("lblancas:: [tkn-api-mbss] :: "+this.getClass().getName()+".compareByPhotos ");
    	log.info("lblancas:: [tkn-api-mbss] :: "+this.getClass().getName()+" Parametros umbral:: "+umbral);
    	log.info("lblancas:: [tkn-api-mbss] :: "+this.getClass().getName()+"Parametros id::::::: "+id);
    	
        Person personPhoto = WSHelper.buildPersonFromPortrait("1111",
                Base64Utils.decodeFromString(b64Photo1), ImageFormatType.JPEG,
                FaceSampleType.STILL);
        Person personPhoto2 = WSHelper.buildPersonFromPortrait("2222",
                Base64Utils.decodeFromString(b64Photo2), ImageFormatType.JPEG,
                FaceSampleType.STILL);
        PersonImage encoded1 = encodePersonForImage(personPhoto, umbral);
        PersonImage encoded2 = encodePersonForImage(personPhoto2, umbral);
        boolean passFirst = encoded1.getScore() >= umbral;
        boolean passSecond = encoded2.getScore() >= umbral;
        if (passFirst && passSecond) {
            return facialMatch(encoded1.getPerson(), encoded2.getPerson());
        }
        return new HttpStatusImageWrapper(HttpStatus.NOT_FOUND, id, "0");
    }

    private HttpStatusWrapper handleFingerDuplicate() {
    	log.info("lblancas:: [tkn-api-mbss] :: "+this.getClass().getName()+".handleFingerDuplicate ");
        log.info("Duplicate finger detected");
        HttpStatusWrapper wrapper = new HttpStatusWrapper(HttpStatus.PRECONDITION_FAILED, "412");
        return wrapper;
    }

    public HttpStatusWrapper findByFace(String faceB64) {
    	log.info("lblancas:: [tkn-api-mbss] :: "+this.getClass().getName()+".findByFace ");
        Person person = WSHelper.buildPersonFromPortrait("tkn-search", Base64Utils.decodeFromString(faceB64), ImageFormatType.JPEG, FaceSampleType.STILL);
        Person encodedPerson = null;
        try {
            encodedPerson = encondePerson(person);//guarda persona
        } catch (DuplicateFingerException e) {
            return handleFingerDuplicate();
        }
        HttpStatusWrapper status = oneToNSearch(encodedPerson);//busca persona
        return status;
    }

    public HttpStatusWrapper findByFaceId(String id, String faceB64) 
    {
    	log.info("lblancas:: [tkn-api-mbss] :: "+this.getClass().getName()+".findByFaceId  id:"+id);
        Person person = WSHelper.buildPersonFromPortrait(id, Base64Utils.decodeFromString(faceB64), ImageFormatType.JPEG, FaceSampleType.STILL);
        Person encodedPerson = null;
        try {
            encodedPerson = encondePerson(person);
        } catch (DuplicateFingerException e) {
            return handleFingerDuplicate();
        }
        HttpStatusWrapper status = oneToOneSearch(encodedPerson, id);
        log.info("HttpStatus response from oneToOne: {}", status);
        if (status.getHttpStatus().equals(HttpStatus.CONFLICT)) {
            return new HttpStatusWrapper(HttpStatus.OK, status.getId());
        } else {
            return status;
        }
    }

    public HttpStatusWrapper findBySlapsAndId(String id, Slaps slaps) {
    	log.info("lblancas:: [tkn-api-mbss] :: "+this.getClass().getName()+".findBySlapsAndId  id:"+id);
        Person person = builFromHands(id, slaps);
        Person encodedPerson = null;
        try {
            encodedPerson = encondePerson(person);
        } catch (DuplicateFingerException e) {
            return handleFingerDuplicate();
        }
        HttpStatusWrapper status = oneToOneSearch(encodedPerson, id);
        log.info("HttpStatus response from oneToOne: {}", status);
        if (status.getHttpStatus().equals(HttpStatus.CONFLICT)) {
            return new HttpStatusWrapper(HttpStatus.OK, status.getId());
        } else {
            return status;
        }
    }

    public HttpStatusWrapper findBySlaps(Slaps slaps) {
    	log.info("lblancas:: [tkn-api-mbss] :: "+this.getClass().getName()+".findBySlaps ");
        Person person = builFromHands("tkn-search", slaps);
        Person encodedPerson = null;
        try {
            encodedPerson = encondePerson(person);
        } catch (DuplicateFingerException e) {
            return handleFingerDuplicate();
        }
        HttpStatusWrapper status = oneToNSearch(encodedPerson);
        return status;
    }

    public HttpStatus deleteById(String id) {	
    	log.info("lblancas:: [tkn-api-mbss] :: "+this.getClass().getName()+".deleteById id:"+id);
        Request request = WSHelper.deletePersonReq(id);
        Response response = wsInvoker.doRequest(request);
        if (response.getDeletePersonResponse().getBasicResponse().getError().getCode().equals(ErrorCode.SUCCESS)) {
            return HttpStatus.OK;
        } else {
            return HttpStatus.INTERNAL_SERVER_ERROR;
        }
    }

    public HttpStatusWrapper findByFingersAndId(Fingers fingers, String id, String imageType) {
    	log.info("lblancas:: [tkn-api-mbss] :: "+this.getClass().getName()+".findByFingersAndId id["+id+"] imageType["+imageType+"]");
        Person personNotEncodedYet = buildFromFingers(id, fingers, imageType);
        Person encodedPerson = null;
        try {
            encodedPerson = encondePerson(personNotEncodedYet);
        } catch (DuplicateFingerException e) {
            return handleFingerDuplicate();
        }
        HttpStatusWrapper status = oneToOneSearch(encodedPerson, id);
        return status;
    }

    public HttpStatusWrapper findByFingers(Fingers fingers, String imageType) {
    	log.info("lblancas:: [tkn-api-mbss] :: "+this.getClass().getName()+".findByFingers  imageType["+imageType+"]");
        Person personNotEncodedYet = buildFromFingers("tkn-search", fingers, imageType);
        Person encondedPerson = null;
        try {
            encondedPerson = encondePerson(personNotEncodedYet);
        } catch (DuplicateFingerException e) {
            return handleFingerDuplicate();
        }
        HttpStatusWrapper status = oneToNSearch(encondedPerson);
        return status;
    }

    public HttpStatusWrapper findByFingersJPEG(Fingers fingers) {
    	log.info("lblancas:: [tkn-api-mbss] :: "+this.getClass().getName()+".findByFingersJPEG ");
        Person personNotEncodedYet = buildFromFingersJPEG("1", fingers);
        Person encondedPerson = null;
        try {
            encondedPerson = encondePerson(personNotEncodedYet);
        } catch (DuplicateFingerException e) {
            e.printStackTrace();
        }
        HttpStatusWrapper status = oneToNSearch(encondedPerson);
        return status;
    }

    public HttpStatus findById(String id) {
    	log.info("lblancas:: [tkn-api-mbss] :: "+this.getClass().getName()+".findById  id:"+id);
        Request req = WSHelper.getPersonFromIdRequest(id);
        Response response = wsInvoker.doRequest(req);
        GetPersonResponse personResponse = response.getGetPersonResponse();
        if (personResponse.getPerson() == null) {
            return HttpStatus.NOT_FOUND;
        }
        return HttpStatus.OK;
    }

    public Person findDetailById(String id) {
    	log.info("lblancas:: [tkn-api-mbss] :: "+this.getClass().getName()+".findDetailById  id:"+id);
        Request req = WSHelper.getPersonFromIdRequest(id);
        Response response = wsInvoker.doRequest(req);
        GetPersonResponse personResponse = response.getGetPersonResponse();
        if (personResponse.getPerson() == null) {
            return null;
        }
        return personResponse.getPerson();
    }


    public HttpStatus checkAndInsertPersonFromSlapsAndFace(String id, Slaps slaps, String face) {
    	log.info("lblancas:: [tkn-api-mbss] :: "+this.getClass().getName()+".checkAndInsertPersonFromSlapsAndFace  id:"+id);
        Person person = builFromHands(id, slaps);
        person = addFace(person, face);
        return requestToEnroll(person);
    }

    public HttpStatus checkAndInsertPersonFromSlaps(String id, Slaps slaps) {
    	log.info("lblancas:: [tkn-api-mbss] :: "+this.getClass().getName()+".checkAndInsertPersonFromSlaps id:"+id);
        log.info("Insert person from slaps");
        Person person = builFromHands(id, slaps);
        log.info("Person to encode...");
        return requestToEnroll(person);
    }

    public HttpStatus checkAndInsertPersonFromFaceAndFingers(String id, String face, Fingers fingers, String imageType) {
    	log.info("lblancas:: [tkn-api-mbss] :: "+this.getClass().getName()+".checkAndInsertPersonFromFaceAndFingers id:"+id + "  , imageType:"+imageType);
        Person personNotEncodedYetOnlyFingers = buildFromFingers(id, fingers, imageType);
        HttpStatus httpStatus = requestToCompare(personNotEncodedYetOnlyFingers);
        log.info("HttpStatus from onlyFingers: {}", httpStatus);

        if (httpStatus != HttpStatus.OK) {
            return HttpStatus.CONFLICT;
        } else {
            Person candidateToEnroll = buildFromFingers(id, fingers, imageType);
            candidateToEnroll = addFace(candidateToEnroll, face);
            httpStatus = requestToEnroll(candidateToEnroll);
            log.info("Candidate to enroll: {}", httpStatus);
            return httpStatus;
        }
    }

    public HttpStatus checkAndInsertPersonFromFace(String id, String face) {
    	log.info("lblancas:: [tkn-api-mbss] :: "+this.getClass().getName()+".checkAndInsertPersonFromFace id:"+id);
        Person personNotEnCodedYet = WSHelper.buildPersonFromPortrait(id, Base64Utils.decodeFromString(face), ImageFormatType.JPEG, FaceSampleType.STILL);
        return requestToEnroll(personNotEnCodedYet);
    }

    public HttpStatus checkAndUpdatePersonFromFace(String id, String face) {
    	log.info("lblancas:: [tkn-api-mbss] :: "+this.getClass().getName()+".checkAndUpdatePersonFromFace id:"+id);
        Person personNotEnCodedYet = WSHelper.buildPersonFromPortrait(id, Base64Utils.decodeFromString(face), ImageFormatType.JPEG, FaceSampleType.STILL);
        return requestToEnroll(personNotEnCodedYet);
    }

    public HttpStatus checkAndInsertPersonFromFingers(String id, Fingers fingers) {
    	log.info("lblancas:: [tkn-api-mbss] :: "+this.getClass().getName()+".checkAndInsertPersonFromFingers id:"+id);
        Person personNotEncodedYet = buildFromFingers(id, fingers);
        return requestToEnroll(personNotEncodedYet);
    }

    public HttpStatus checkAndInsertPersonFromFingers(String id, Fingers fingers, String imageType) {
    	log.info("lblancas:: [tkn-api-mbss] :: "+this.getClass().getName()+".checkAndInsertPersonFromFingers  id:"+id);
        Person personNotEncodedYet = buildFromFingers(id, fingers, imageType);
        return requestToEnroll(personNotEncodedYet);
    }

    private Person addFace(Person source, String faceB64) {
    	log.info("lblancas:: [tkn-api-mbss] :: "+this.getClass().getName()+".addFace(Person source, String faceB64) ");
        FaceSample sample = new FaceSample();
        StillImage stillImage = new StillImage();
        stillImage.setImage(WSHelper.imageFromByte(Base64Utils.decodeFromString(faceB64), ImageFormatType.JPEG));
        sample.setStillImage(stillImage);
        sample.setSampleType(FaceSampleType.STILL);
        Registration reg = source.getRegistration().get(0);
        reg.getFaceSample().add(sample);
        source.getRegistration().remove(0);
        source.getRegistration().add(reg);
        return source;

    }

    private Person builFromHands(String id, Slaps slaps) {
    	log.info("lblancas:: [tkn-api-mbss] :: "+this.getClass().getName()+".builFromHands(String id, Slaps slaps) ");
        Person person = WSHelper.buildPersonByHand(id, Base64Utils.decodeFromString(slaps.getLs().trim()),
                Base64Utils.decodeFromString(slaps.getRs().trim()), Base64Utils.decodeFromString(slaps.getTs().trim()), FingerprintSampleType.SLAP_442);
        return person;
    }

    private Person buildFromFingers(String id, Fingers fingers) {
    	log.info("lblancas:: [tkn-api-mbss] :: "+this.getClass().getName()+".buildFromFingers(String id, Fingers fingers) ");
        FingersBytes fingersBytes = new FingersBytes(fingers);
        Person personNotEncodedYet = WSHelper.buildPersonFromFingers(id, fingersBytes.getRt(), fingersBytes.getRi(),
                fingersBytes.getRm(), fingersBytes.getRr(), fingersBytes.getRl(), fingersBytes.getLt(),
                fingersBytes.getLi(), fingersBytes.getLm(), fingersBytes.getLr(), fingersBytes.getLl(),
                ImageFormatType.WSQ, FingerprintSampleType.TENPRINT_SLAP);
        return personNotEncodedYet;
    }

    private Person buildFromFingersJPEG(String id, Fingers fingers) {
    	log.info("lblancas:: [tkn-api-mbss] :: "+this.getClass().getName()+".buildFromFingersJPEG(String id, Fingers fingers)  ");
        FingersBytes fingersBytes = new FingersBytes(fingers);
        Person personNotEncodedYet = WSHelper.buildPersonFromFingers(id, fingersBytes.getRt(), fingersBytes.getRi(),
                fingersBytes.getRm(), fingersBytes.getRr(), fingersBytes.getRl(), fingersBytes.getLt(),
                fingersBytes.getLi(), fingersBytes.getLm(), fingersBytes.getLr(), fingersBytes.getLl(),
                ImageFormatType.JPEG, FingerprintSampleType.TENPRINT_SLAP);
        return personNotEncodedYet;
    }

    private Person buildFromFingers(String id, Fingers fingers, String imageType) {
    	log.info("lblancas:: [tkn-api-mbss] :: "+this.getClass().getName()+".buildFromFingers(String id, Fingers fingers, String imageType) ");
        ImageFormatType imageType1 = ImageFormatType.valueOf(imageType.trim().toUpperCase());
        FingersBytes fingersBytes = new FingersBytes(fingers);
        Person personNotEncodedYet = WSHelper.buildPersonFromFingers(id, fingersBytes.getRt(), fingersBytes.getRi(),
                fingersBytes.getRm(), fingersBytes.getRr(), fingersBytes.getRl(), fingersBytes.getLt(),
                fingersBytes.getLi(), fingersBytes.getLm(), fingersBytes.getLr(), fingersBytes.getLl(),
                imageType1, FingerprintSampleType.TENPRINT_SLAP);
        return personNotEncodedYet;
    }

    private HttpStatus requestToCompare(Person personNotEncodedYet) {
    	log.info("lblancas:: [tkn-api-mbss] :: "+this.getClass().getName()+".requestToCompare(Person personNotEncodedYet) ");
        Person encodedPerson = null;
        try {
            encodedPerson = encondePerson(personNotEncodedYet);
        } catch (DuplicateFingerException e) {
            log.error("Duplicate fingers detected: {}", e.getMessage());
            return HttpStatus.PRECONDITION_FAILED;
        }
        log.info("Person encoded ok");
        if (encodedPerson == null) {
            return HttpStatus.BAD_REQUEST;
        }
        //1 to 1 search
        HttpStatusWrapper searchStatus = oneToNSearch(encodedPerson);
        log.info("One to N Status in requestToCompare: {}", searchStatus);
        if (!searchStatus.getHttpStatus().equals(HttpStatus.OK)) {
            return searchStatus.getHttpStatus();
        }
        return searchStatus.getHttpStatus();
    }

    private HttpStatus requestToEnroll(Person personNotEncodedYet) {
    	log.info("lblancas:: [tkn-api-mbss] :: "+this.getClass().getName()+".requestToEnroll(Person personNotEncodedYet) ");
        log.info("Request for enrollment");
        Person encodedPerson = null;
        try {
            encodedPerson = encondePerson(personNotEncodedYet);
        } catch (DuplicateFingerException e) {
            log.error("Duplicate fingers detected: {}", e.getMessage());
            return HttpStatus.PRECONDITION_FAILED;
        }
        log.info("Person encoded ok");
        if (encodedPerson == null) {
            return HttpStatus.BAD_REQUEST;
        }
        //1 to 1 search
        HttpStatusWrapper searchStatus = oneToNSearch(encodedPerson);
        log.info("One to N Status: {}", searchStatus);
        if (!searchStatus.getHttpStatus().equals(HttpStatus.OK)) {
            return searchStatus.getHttpStatus();
        }
        return insertPerson(encodedPerson);
    }

    private HttpStatus insertPerson(Person person) {
    	log.info("lblancas:: [tkn-api-mbss] :: "+this.getClass().getName()+".insertPerson(Person person) ");
        log.info("Insert person request...");
        Request insertRequest = WSHelper.insertPersonReq(person);
        Response insertResponse = wsInvoker.doRequest(insertRequest);
        log.info("Insert person");
        log.info("Search response: {}", insertResponse);
        log.info("Search error response: {}", insertResponse.getErrorResponse());
        log.info("Search error response basic response: {}", insertResponse.getInsertPersonResponse().getBasicResponse());
        log.info("Search error response basic response error code: {}", insertResponse.getInsertPersonResponse().getBasicResponse().getError());
        if (!insertResponse.getInsertPersonResponse().getBasicResponse().getError().getCode().equals(ErrorCode.SUCCESS)) {
            return HttpStatus.INTERNAL_SERVER_ERROR;
        }
        return HttpStatus.OK;
    }

    private PersonImage encodePersonForImage(Person personToEncode, int umbral) {
    	log.info("lblancas:: [tkn-api-mbss] :: "+this.getClass().getName()+".encodePersonForImage(Person personToEncode, int umbral)     UMBRAL+++++++++++");
        Request encodeRequest = WSHelper.codePersonReq(personToEncode, false);
        Response encodeResponse = wsInvoker.doRequest(encodeRequest);
        if (!encodeResponse.getEncodePersonResponse().getBasicResponse().getError().getCode().equals(ErrorCode.SUCCESS)) {
            log.info("Error encoding person: {} {} ", encodeResponse.getEncodePersonResponse().getBasicResponse().getError().getCode(), encodeResponse.getEncodePersonResponse().getBasicResponse().getError().getMessage());
            return null;
        }
        Person encodedPerson = encodeResponse.getEncodePersonResponse().getPerson();
        int registered = encodedPerson.getRegistration().get(0)
                .getFaceSample().get(0).getTemplate()
                .getQuality();
        return new PersonImage(encodedPerson, registered);
    }

    private JSONArray validateSecuence(EncodePersonResponse response) {
    	log.info("lblancas:: [tkn-api-mbss] :: "+this.getClass().getName()+".validateSecuence(EncodePersonResponse response) ");
        JSONArray results = null;

        List<SequenceCheckError> errors = response.getSequenceCheckErrors();
        log.info("validateSecuence step 1 {}", errors.size());

        if (errors != null && !errors.isEmpty()) {

            log.info("validateSecuence step 2");
            String hand = null;
            String finger = null;
            results = new JSONArray();
            for (SequenceCheckError err : errors) {
                log.info("validateSecuence step 3");
                String value = "";
                for (SequenceCheckErrorItem item : err.getItems()) {
                    log.info("validateSecuence step 4");
                    hand = item.getDecodedPosition().getPrintPosition().getHandPosition().value();
                    finger = item.getDecodedPosition().getPrintPosition().getFingerPosition().value();

                    JSONObject jo = new JSONObject();
                    try {
                        jo.put("hand", hand);
                        jo.put("finger", finger);
                    } catch (JSONException e) {
                        log.error("Error building JSON block for duplicated fingers");
                    }
                    //value = value + "Duplicated in hand: " + hand + " finger: " + finger + " - ";
                    value = "" + jo;
                    //System.out.println("Value: " + value);
                    //results.put(value);
                }
                log.info("Value: {}", value);
                results.put(value);
            }
        }
        return results;
    }

    private Person encondePerson(Person personToEncode) throws DuplicateFingerException {
    	log.info("lblancas:: [tkn-api-mbss] :: "+this.getClass().getName()+".encondePerson(Person personToEncode) ");
        Request encodeRequest = WSHelper.codePersonReq(personToEncode, true);
        Response encodeResponse = wsInvoker.doRequest(encodeRequest);
        try {
            if (!encodeResponse.getEncodePersonResponse().getBasicResponse().getError().getCode().equals(ErrorCode.SUCCESS)) {
                log.info("Error encoding person: {} {} ", encodeResponse.getEncodePersonResponse().getBasicResponse().getError().getCode(), encodeResponse.getEncodePersonResponse().getBasicResponse().getError().getMessage());
                return null;
            }
        } catch (Exception e) {
            log.error("Error getting error code from encondePersonRequest with message: {}", e.getMessage());
            log.error("Error trace: {}", e);
        }
        try {
            //TODO improve if statements
            if (encodeResponse.getEncodePersonResponse().getSequenceCheckErrors().size() == 0) {
                log.info("No sequence check errors found, continue");
            }else if(!encodeResponse.getEncodePersonResponse().getSequenceCheckErrors().isEmpty()){
                boolean isDuplicated = false;
                for (SequenceCheckError seq :
                        encodeResponse.getEncodePersonResponse().getSequenceCheckErrors()) {
                    if(seq.getType().equals(SequenceCheckErrorType.DUPLICATE_ITEM)){
                        isDuplicated = true;
                    }
                }
                if(isDuplicated){
                    JSONArray validatedSequence = validateSecuence(encodeResponse.getEncodePersonResponse());
                    throw new DuplicateFingerException("DUPLICATED_FINGERS", validatedSequence);
                }
            }
        } catch (Exception e) {
            log.error("Error looking for SequenceCheckErrors");
            return null;
        }
        Person encodedPerson = encodeResponse.getEncodePersonResponse().getPerson();
        return encodedPerson;
    }

    private HttpStatusWrapper oneToOneSearch(Person personEncoded, String id) {
    	log.info("lblancas:: [tkn-api-mbss] :: "+this.getClass().getName()+".oneToOneSearch(Person personEncoded, String id) ");
        Request verifyreq = WSHelper.verifReq(personEncoded, id);
        Response searchResponse = wsInvoker.doRequest(verifyreq);
        log.info("One to One search");
        if (searchResponse.getErrorResponse() == null && searchResponse.getAuthenticatePersonResponse() != null) {
            Integer hitRank = searchResponse.getAuthenticatePersonResponse().getNoHitRank();
            log.info("Hit rank: {}", hitRank);
            if (hitRank != null) {
                if (hitRank.equals(-1) || hitRank.equals(0)) {
                    log.info("No hit rank found");
                    return new HttpStatusWrapper(HttpStatus.NOT_FOUND, id);
                }
            } else {
                log.info("Hit rank is null, continue");
            }
            List<Candidate> listCandidates = searchResponse.getAuthenticatePersonResponse().getCandidate();
            if (CollectionUtils.isEmpty(listCandidates)) {
                log.info("No candidate list");
                return new HttpStatusWrapper(HttpStatus.NOT_FOUND, id);
            }
            Candidate principal = listCandidates.get(0);
            if (principal.getDecision().value().toUpperCase().contains("NO_HIT")) {
                return new HttpStatusWrapper(HttpStatus.NOT_FOUND, id);
            }
            String idFound = principal.getId();
            return new HttpStatusWrapper(HttpStatus.OK, idFound);
        } else {
            log.info("Error response present or AuthenticateResponse null");
            return new HttpStatusWrapper(HttpStatus.NOT_FOUND, id);
        }
    }

    private HttpStatusImageWrapper facialMatch(Person person, Person personRef) {
    	log.info("lblancas:: [tkn-api-mbss] :: "+this.getClass().getName()+".facialMatch(Person person, Person personRef) ");
        Request request = WSHelper.verifReq(person, personRef);
        Response searchResponse = wsInvoker.doRequest(request);
        if (searchResponse.getErrorResponse() == null && searchResponse.getPersonToPersonAuthResponse() != null) {
            if (!searchResponse.getPersonToPersonAuthResponse().getBasicResponse().getError().getCode().equals(ErrorCode.SUCCESS)) {
                return new HttpStatusImageWrapper(HttpStatus.NOT_FOUND, person.getId(), "0");
            }
            Integer matchRank = searchResponse.getPersonToPersonAuthResponse().getNoHitRank();
            List<String> ids = new ArrayList<>();
            Integer score = null;
            for (Candidate candidate : searchResponse.getPersonToPersonAuthResponse().getCandidate()) {
                if (candidate.getDecision() == Decision.HIT) {
                    score = candidate.getScore();
                    ids.add(candidate.getId());
                }
            }
            if (ids.isEmpty()) {
                return new HttpStatusImageWrapper(HttpStatus.NOT_FOUND, person.getId(), "0");
            }
            return new HttpStatusImageWrapper(HttpStatus.OK, ids.get(0), String.valueOf(score));
        }
        return new HttpStatusImageWrapper(HttpStatus.NOT_FOUND, person.getId(), "0");
    }

    private HttpStatusWrapper oneToNSearch(Person personEncoded) {
    	log.info("lblancas:: [tkn-api-mbss] :: "+this.getClass().getName()+".oneToNSearch(Person personEncoded) ");
        Request searchReq = WSHelper.searchPPReq(personEncoded);
        Response searchResponse = wsInvoker.doRequest(searchReq);
        if (searchResponse.getErrorResponse() == null && searchResponse.getPersonToPersonMatchResponse() != null) {
            if (!searchResponse.getPersonToPersonMatchResponse().getBasicResponse().getError().getCode().equals(ErrorCode.SUCCESS)) {
                return new HttpStatusWrapper(HttpStatus.BAD_REQUEST, null);
            }
            PersonToPersonMatchResponse matchResponse = searchResponse.getPersonToPersonMatchResponse();
            if (!matchResponse.getNoHitRank().equals(0)) {
                String id = matchResponse.getCandidate().get(0).getId();
                return new HttpStatusWrapper(HttpStatus.CONFLICT, id);
            }
            return new HttpStatusWrapper(HttpStatus.OK, null);
        }
        return new HttpStatusWrapper(HttpStatus.BAD_REQUEST, null);
    }

    private class PersonImage 
    {
    
        private Person person;
        private int score;

        PersonImage(Person person, int score) {
            this.person = person;
            this.score = score;
        }

        public Person getPerson() {
            return person;
        }

        public void setPerson(Person person) {
            this.person = person;
        }

        public int getScore() {
            return score;
        }

        public void setScore(int score) {
            this.score = score;
        }
    }
}
