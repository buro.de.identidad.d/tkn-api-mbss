# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [1.0.0] - 2018-05-22
### Released
- Exposes business methods (REST based API) for SOAP WS related to the MBSS specification

## [1.0.1] - 2018-05-24
 - Exposes delete method by id in REST (DELETE) json input format
 
## [1.0.2] - 2018-06-08
 - FIX the bad error handling when diferent error than FINGERS_DUPLICATED is raised when encoding person 